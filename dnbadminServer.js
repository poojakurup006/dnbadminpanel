var express         =         require("express");
var mysql           =         require("mysql");
var app             =         express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var request = require('request');
//app.use(bodyParser());
/*
  * Configure MySQL parameters.
*/
var PDFDocument = require('pdfkit'); 
var pdf = require('html-pdf');                     
var fs=require('fs');
var connection      =         mysql.createConnection({
        host        :         "127.0.0.1",
        user        :         "root",
        password    :         "Mini@123",
        database    :         "bakeryndairy",
        port        :         "3306",
        multipleStatements: true

});

connection.connect(function(error){
  if(error)
    {
      console.log("Problem with MySQL"+error);
    }
  else
    {
      console.log("Connected with Database");
    }
});

/*
  * Configure Express Server.
*/

app.use(express.static(__dirname + '/angular'));
app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,data");
  next();
});
/*
  * Define routing of the application.
*/

app.get('/',function(req,res){
  res.sendfile('index.html');
});

app.get('/getFrequency',function(req,res){

  connection.query("SELECT * FROM tbl_frequency",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetAllState',function(req,res){

  connection.query("SELECT * FROM tbl_state order by name ASC",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

app.get('/GetCityName',function(req,res){

  connection.query("SELECT * FROM tbl_city order by name ASC",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetRoleWiseCityName/:id',function(req,res){
  var mobile=JSON.parse(req.params.id);

  connection.query("SELECT c.* FROM tbl_admin_registration ar join tbl_admin_role_city arc on arc.admin_id=ar.id join tbl_city c on c.id=arc.city_id where ar.mobile = ?",mobile,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/getAdminId/:id',function(req,res){
  var mobile=JSON.parse(req.params.id);

  connection.query("Select id from tbl_admin_registration where mobile = ?",mobile,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetAllMasterCity',function(req,res){
  var state=req.query.state;
  var tm =[state];

  connection.query("SELECT * FROM tbl_master_city order by name ASC",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetMasterCity',function(req,res){
  var state=req.query.state;
  var tm =[state];

  connection.query("SELECT * FROM tbl_master_city where state_id=? order by name ASC",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetMasterCityWiseCity',function(req,res){
  var city=req.query.city;
  var tm =[city];

  connection.query("SELECT * FROM tbl_city where master_city_id=? order by name ASC",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetAreaName',function(req,res){
  var city=req.query.city;
  var tm =[city];

  connection.query("SELECT * FROM tbl_area where city_id=? order by name ASC",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/getAllRole',function(req,res){

  connection.query("SELECT * FROM tbl_role",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/getAllDeliveryTime',function(req,res){

  connection.query("SELECT * FROM tbl_delivery_time",function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/addNewState',function(req,res){
  var state =JSON.parse(req.get('data'));
  var tm ={'name':state};

  connection.query("INSERT INTO tbl_state set ?",  tm ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/addNewMasterCity',function(req,res){
  var data =JSON.parse(req.get('data'));
  var tm ={'name':data.name,'state_id':data.state_id};

  connection.query("INSERT INTO tbl_master_city set ?",  tm ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/addNewCity',function(req,res){
  var data =JSON.parse(req.get('data'));
  var tm ={'name':data.name,'state_id':data.state_id,'master_city_id':data.master_city_id};

  connection.query("INSERT INTO tbl_city set ?",  tm ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/addNewArea',function(req,res){
  var data =JSON.parse(req.get('data'));
  //var tm ={'name':data.name,'city_id':data.city_id,'state_id':data.state_id};

  connection.query("INSERT INTO tbl_area set ?",  data ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/AddDeliveryTime',function(req,res){
  var time =JSON.parse(req.get('data'));
  var tm ={'name':time};

  connection.query("INSERT INTO tbl_delivery_time set ?",  tm ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/AddAreaDeliveryTime',function(req,res){
  var deliverydata =JSON.parse(req.get('data'));
  var tm =[];
  for(i=0;i<deliverydata.length;i++)
  {
    tm.push([deliverydata[i].delivery_time_id,deliverydata[i].area_id])
  }

  connection.query("INSERT INTO tbl_area_delivery_time (delivery_time_id,area_id) values ?",  [tm] ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetDeliveryBoyName',function(req,res){
  var city=req.query.city;
  var tm =[city];
  
  
  connection.query("SELECT dr.* FROM tbl_deliveryboy_registration dr join tbl_city c on c.id=dr.city where c.name = ? AND dr.flag='E' order by dr.name ASC",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/getVegetables',function(req,res){
  //var varStaff =req.params.Staff;
  
  //console.log(varStaff);
 // connection.query("SELECT * FROM rentmanagement.tbl_staff" ,function(err,rows){
   connection.query("CALL sp_get_dairy()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          //console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows[0]));
        }
  });
});
app.get('/getFruits',function(req,res){
  var varStaff =req.params.Staff;
  
 // console.log(varStaff);
 // connection.query("SELECT * FROM rentmanagement.tbl_staff" ,function(err,rows){
   connection.query("CALL sp_get_bakery()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows[0]));
        }
  });
});
app.get('/getVegetablePrices',function(req,res){
  //var varStaff =req.params.Staff;
   var AuthData =JSON.parse(req.get('data'));
    console.log(AuthData);
  connection.query("SELECT d.id id, d.name name,d.hindi_name,di.price price,u.name quan, u.unit unit FROM tbl_customer_registration cus INNER JOIN tbl_dairy_inventory di ON cus.city=di.city INNER JOIN tbl_dairy d ON d.id=di.dairy_id INNER JOIN tbl_unit u ON u.id=di.quan WHERE cus.mobile=?",AuthData ,function(err,rows){
   //connection.query("CALL sp_get_vegetable()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/getFruitPrices',function(req,res){
  //var varStaff =req.params.Staff;
   var AuthData =JSON.parse(req.get('data'));
    console.log(AuthData);
  connection.query("SELECT b.id id, b.name name,b.hindi_name, bi.price price,u.name quan, u.unit unit FROM tbl_customer_registration cus INNER JOIN tbl_bakery_inventory bi ON cus.city=bi.city INNER JOIN tbl_bakery b ON b.id=bi.bakery_id  INNER JOIN tbl_unit u ON u.id=bi.quan WHERE cus.mobile=?",AuthData ,function(err,rows){
   //connection.query("CALL sp_get_vegetable()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/CheckVegUpdatedRate',function(req,res){
  
  var city=req.query.city;
  var date=req.query.date;
  var tm =[city,date];
  
  //console.log(varLeadId);
  connection.query("Select count(*) from tbl_dairy_updated_rate where city = ? and date = ?",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/CheckFruitUpdatedRate',function(req,res){
  
  var city=req.query.city;
  var date=req.query.date;
  var tm =[city,date];
  
  //console.log(varLeadId);
  connection.query("Select count(*) from tbl_bakery_updated_rate where city = ? and date = ?",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/SetSubscriptionOrderDeliverBoy',function(req,res){
  var orderDetails =JSON.parse(req.get('data'));

  console.log("==================>"+req.get('data'));
  connection.query("INSERT INTO `tbl_subscriptionorder_n_deliveryboy` (subscription_order_id,deliveryboy_id) values ?",  [orderDetails] ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/SetOrderDeliverBoy',function(req,res){
  var orderDetails =JSON.parse(req.get('data'));

  console.log("==================>"+req.get('data'));
  connection.query("INSERT INTO `tbl_order_n_deliveryboy` (order_id,deliveryboy_id) values ?",  [orderDetails] ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetVegDetails',function(req,res){
  
  var city=req.query.city;
  var tm =[city];
  
  //console.log(varLeadId);
  connection.query("Select d.id,d.image,d.name,d.hindi_name,di.price,c.name as cityname from tbl_dairy d inner join tbl_dairy_inventory di on di.dairy_id=d.id inner join tbl_city c on c.id=di.city where c.name like ?",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetFruitsDetails',function(req,res){
 
  var city=req.query.city;
  var tm =[city];
  
  //console.log(varLeadId);
  connection.query("Select b.id,b.image,b.name,b.hindi_name,bi.price,c.name as cityname from tbl_bakery b inner join tbl_bakery_inventory bi on bi.bakery_id=b.id inner join tbl_city c on c.id=bi.city where c.name like ?",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetMasterVegDetails',function(req,res){
  var city=req.query.city;
  var date=req.query.date;
  var tm =[city,date];
  
  connection.query("select dur.id,dur.dairy_id,dur.name,d.hindi_name,dur.price,dur.supplier_price,dur.todays_price,dur.date,dur.city,c.id as c_id from tbl_dairy_updated_rate dur join tbl_dairy d on d.id=dur.dairy_id join tbl_city c on c.name=dur.city where c.name = ? and dur.date = ? and dur.status='pending' and dur.todays_price IS NOT NULL",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/GetMasterFruitDetails',function(req,res){
  var city=req.query.city;
  var date=req.query.date;
  var tm =[city,date];
  
  connection.query("select bur.id,bur.bakery_id,bur.name,b.hindi_name,bur.price,bur.supplier_price,bur.todays_price,bur.date,bur.city,c.id as c_id from tbl_bakery_updated_rate bur join tbl_bakery b on b.id=bur.bakery_id join tbl_city c on c.name=bur.city where c.name = ? and bur.date =? and bur.status='pending' and bur.todays_price IS NOT NULL",tm,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/InsertVegDetails',function(req,res){
  var vegDetails =JSON.parse(req.get('data'));
  var test=[];
  for(i=0;i<vegDetails.length;i++)
  {
  test.push([vegDetails[i].id, vegDetails[i].name, vegDetails[i].price,vegDetails[i].supplier_price,vegDetails[i].todays_price,vegDetails[i].date,vegDetails[i].city])
  
  }
  console.log("==================>"+JSON.stringify(req.get('data')));
  connection.query("INSERT INTO `tbl_dairy_updated_rate` (dairy_id,name,price,supplier_price,todays_price,date,city) values ?",  [test] ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/InsertFruitDetails',function(req,res){
  var vegDetails =JSON.parse(req.get('data'));
  var test=[];
  for(i=0;i<vegDetails.length;i++)
  {
  test.push([vegDetails[i].id, vegDetails[i].name, vegDetails[i].price,vegDetails[i].supplier_price,vegDetails[i].todays_price,vegDetails[i].date,vegDetails[i].city])
  
  }
  console.log("==================>"+JSON.stringify(req.get('data')));
  connection.query("INSERT INTO `tbl_bakery_updated_rate` (bakery_id,name,price,supplier_price,todays_price,date,city) values ?",  [test] ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.post('/UpdateMasterVegDetails',function(req,res){
  var vegDetails =JSON.parse(req.get('data'));
  var test=[];

  for(i=0;i<vegDetails.length;i++)
  {
    var test=[];
  test.push([vegDetails[i].todays_price]);
  test.push([vegDetails[i].dairy_id]);
  test.push([vegDetails[i].c_id]);
  test.push([vegDetails[i].city]);
  test.push([vegDetails[i].date]);
  

  connection.query("call sp_approve_dairy_price(?,?,?,?,?)", test ,function(err,rows)
  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
         res.end(JSON.stringify(rows));

        }
  });
  }

});
app.post('/UpdateMasterFruitDetails',function(req,res){
  var vegDetails =JSON.parse(req.get('data'));
  var test=[];

  for(i=0;i<vegDetails.length;i++)
  {
    var test=[];
  test.push([vegDetails[i].todays_price]);
  test.push([vegDetails[i].bakery_id]);
  test.push([vegDetails[i].c_id]);
  test.push([vegDetails[i].city]);
  test.push([vegDetails[i].date]);
  
  connection.query("call sp_approve_bakery_price(?,?,?,?,?)", test ,function(err,rows)
  /*connection.query(editsqlquery, [test] ,function(err,rows)*/
  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
         res.end(JSON.stringify(rows));

        }
  });
  }

});
app.post('/UpdateMaterStatus',function(req,res){
  var vegDetails =JSON.parse(req.get('data'));
  var updateStatus=[];

  for(i=0;i<vegDetails.length;i++)
  {
   var updateStatus=[];
  updateStatus.push([vegDetails[i].dairy_id]);
  updateStatus.push([vegDetails[i].city]);
  
  connection.query("UPDATE tbl_dairy_updated_rate SET status = 'approved' WHERE dairy_id = ? and city = ?", updateStatus ,function(err,rows)
    
    {
      if(err)
        {
          console.log("Problem with MySQL"+err);
        }
        else
          {
            res.end(JSON.stringify(rows));
            
          }
    });

  }

});
app.post('/UpdateMaterFruitStatus',function(req,res){
  var vegDetails =JSON.parse(req.get('data'));
  var updateStatus=[];

  for(i=0;i<vegDetails.length;i++)
  {
   var updateStatus=[];
  updateStatus.push([vegDetails[i].bakery_id]);
  updateStatus.push([vegDetails[i].city]);
  
  connection.query("UPDATE tbl_bakery_updated_rate SET status = 'approved' WHERE bakery_id = ? and city = ?", updateStatus ,function(err,rows)
    
    {
      if(err)
        {
          console.log("Problem with MySQL"+err);
        }
        else
          {
            res.end(JSON.stringify(rows));
            
          }
    });

  }

});

app.get('/GetAllLeadDetails',function(req,res)
{
  var city=req.query.city;
  var tm =[city];
 // console.log(req.query);
 connection.query("SELECT o.*, ct.name cityname FROM tbl_order o INNER JOIN tbl_customer_registration cr  ON cr.mobile = o.mobile INNER JOIN tbl_city ct  ON cr.city = ct.id WHERE o.status = 'pending' AND YEAR(o.date) = YEAR(NOW()) AND MONTH(o.date) = MONTH(NOW()) AND DAY(o.date) = DAY(NOW()) and ct.name like ?",tm,function(err,rows)
  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        res.end(JSON.stringify(rows));
      }
  });

});


app.post('/doSignUp',function(req,res){
  var varSignUpData =JSON.parse(req.get('data'));
  varSignUpData.date =new Date();
  var emailNmobileCheck=[varSignUpData.mobile];
   
 console.log("from data  :"+JSON.stringify(emailNmobileCheck));

  connection.query("select mobile from tbl_admin_registration where  mobile=?",emailNmobileCheck  ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+ err);
        res.status(500).send("some problem" + err);
      }
      else if (rows.length>0){
        console.log(rows.length + " already email/mobile "+JSON.stringify(rows));
        res.status(202).send("Mobile Already Exists. Please use a different mobile.");
      }
      else{

            console.log(rows.length + " No Mobile"+JSON.stringify(rows));
            connection.query("INSERT INTO tbl_admin_registration SET  ?",varSignUpData  ,function(err,rows){
            if(err)
              { 
                console.log("Problem with MySQL"+ err);
                res.status(500).send("some problem" + err);
              }

              else
                {
                  console.log(rows.length + " inside last else"+JSON.stringify(rows));
                  res.end(JSON.stringify(rows));
                }
            });

      }

    });

});
app.get('/GetloginDetails/:id/:password',function(req,res){
  var email =JSON.parse(req.params.id);
  var password =JSON.parse(req.params.password);
  var temp =[];
  temp.push(email);
  temp.push(password);
  
  console.log(req.params.password);
  //connection.query("SELECT email,memberID,passMD5 FROM tbl_membership_users where email = ?  and passMD5 = ?",  temp ,function(err,rows){
   connection.query("SELECT mobile as email , id,role, password as passMD5 FROM tbl_admin_registration where mobile = ? and password = ? ", temp,  function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        { console.log(rows);
          res.end(JSON.stringify(rows[0]));


        }
  });
});

app.get('/recoverPasswordonMobile',function(req,res){
   var mobile =JSON.parse(req.get('data'));
    console.log("server siide log for alldata" + req.get('data'));
  connection.query("select name,password FROM tbl_admin_registration where mobile =?",mobile ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
          if(rows.length>0 && rows[0].password !='' && mobile!=''){
          var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}};
          var password=Base64.decode(rows[0].password);
          var testURL = "http://smsc.biz/httpapi/send?username=gargsharad19@gmail.com&password=SMS123&sender_id=DNBBAZ&route=T&phonenumber="+ mobile + "&message=Dear%20Customer%20Your%20Password%20is%20%23"+ password+"%23.%20DairynBakeryBazar.";
          console.log(testURL);
          request(testURL, function (error, response, body) { 
                      if (!error && response.statusCode == 200) {
                        console.log(body) ;// Print the google web page.
                      }
                      else{
                        console.log(JSON.stringify(error));
                        console.log(response.statusCode);
                      }
                    });
        }
      }
  });
});


app.get('/doLogin',function(req,res){
  var user =JSON.parse(req.get('data'));
   var temp =[];
  temp.push(user.mobile);
  temp.push(user.password);
  ///temp.push(1);
  console.log(""+user.password);
 // connection.query("SELECT * FROM rentmanagement.tbl_staff" ,function(err,rows){
   connection.query("SELECT mobile , id, password FROM tbl_admin_registration where mobile = ? and password = ? ", temp,  function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});


app.get('/getMenuDetails/:id',function(req,res){
  var mobile=JSON.parse(req.params.id);

  console.log("test menu email id---------- "+ mobile);

  connection.query("select rm.menu_id from tbl_admin_registration ar join tbl_role_menu rm on rm.role_id=ar.role where ar.mobile = ?",mobile,function(err,rows){

    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});

       function formatMenu (rows){
        
        var sb = '';
        var mainMenuPrefix ="<li  data-toggle='collapse'  class='collapsed'";
        var mainMenuPrefixEnd =">";
        var subMenuPrefix ="<ul class='sub-menu collapse'";
        var subMenuPrefixEnd =">";
        var mainMenuSuffix ="</li>";
        var subMenuSuffix ="</ul>";
        var mainmenu=true;
        var previous="test";
        
        
for (var i = 0; i < rows.length; i++) {
 // alert(rows[i]["menuName"]);
      if(mainmenu && ! (rows[i]["menuName"]===previous) ){
        //alert(rows[i]["menuName"]);
        if(previous!="test"){
                  sb= sb + subMenuSuffix;
                  //sb=sb+mainMenuSuffix;
                }
        sb=sb+mainMenuPrefix + "  data-target='#" +rows[i]["menuName"]+"'"+mainMenuPrefixEnd;
      sb=sb+rows[i]["menuHTML"];
      sb=sb+mainMenuSuffix;
       sb=sb+subMenuPrefix + "  id='" +rows[i]["menuName"]+"'"+subMenuPrefixEnd;

      }
       previous=rows[i]["menuName"];
           sb=sb+rows[i]["subMenuHTML"];
          if(previous !="test" && ! rows[i]["menuName"]===previous) {
            sb=sb+subMenuSuffix;
           // sb=sb+mainMenuSuffix;
            
          }

    } //end of for
            sb=sb+subMenuSuffix;
           //sb=sb+mainMenuSuffix;
            
            console.log(sb);
return sb;

    } // end  of funcntion

app.get('/addsubscription',function(req,res){
  var subdata=JSON.parse(req.get('data'));
  connection.query("INSERT INTO tbl_subscription SET  ?",subdata ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/getSubcription',function(req,res){
  //var varStaff =req.params.Staff;
   var AuthData =JSON.parse(req.get('data'));
    console.log(AuthData);
  connection.query("CALL sp_get_subscription(?)",AuthData ,function(err,rows){
     if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/getException',function(req,res){
  //var varStaff =req.params.Staff;
  var Adata=[];
   var AuthData =JSON.parse(req.get('data'));
   var status = "active";
   Adata.push(AuthData);
   Adata.push(status);

    console.log(AuthData);
  connection.query("SELECT * FROM tbl_exception where subscription_id =? and estatus =?", Adata ,function(err,rows){
     if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log(JSON.stringify(rows));
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/saveExceptionDate',function(req,res){
  var exceptiondata=JSON.parse(req.get('data'));
  var exceptionsDates=exceptiondata.date;
  var id=exceptiondata.id;
  
  var TopWrap=[];
   var bulkRecords=[];
   for(var i=0;i<exceptionsDates.length;i++){
    var row=[];
    row.push(id);
    row.push(exceptionsDates[i]);
   // bulkRecords.push(row); 
     connection.query("call sp_add_exception(?,?)",row ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          
          res.end(JSON.stringify(rows));
          
        }
    });   
   }

  });
app.get('/confirmOrder',function(req,res){
  var orderData=JSON.parse(req.query.data);
   var orderDate=new Date();
  // console.log(orderdate);
   orderData.date=orderDate;
   console.log("server siide OrderData " +JSON.stringify(orderData) );

    console.log("server siide log for alldata" + req.get('data'));
  connection.query("INSERT INTO tbl_order SET  ?",orderData ,function(err,rows){
   //connection.query("CALL sp_get_vegetable()" ,function(err,rows){
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          console.log("order============"+JSON.stringify(rows));
          res.end(JSON.stringify(rows));
          sendEmail(orderData);
        }
  });
});
var sendPasswordEmail=function(pwd,email,name){
  console.log(email);
var tempHTML ="Hi " +  name +",<br/><br/> Your Password is<strong>"+ pwd+" </strong><br/><br/> "
            
            tempHTML= tempHTML+ " <h2>Vegetablebazar </h2><br> Love to eat vegetables and fruits everyday.<br> we hope to see you again";
             tempHTML= tempHTML+ " For any queries/complain: <br>";
             tempHTML= tempHTML+ "mail us :- support@vegetablebazar.com <br>";
             var nodemailer = require("nodemailer");

            var smtpTransport = nodemailer.createTransport("SMTP",{
               service: "Gmail",
               auth: {
                   user: "vegetablebazarapp@gmail.com",
                   pass: "Company@2016"
               }
            });

            smtpTransport.sendMail({
               from: "vegetable bazar <vegetablebazarapp@gmail.com>", // sender address
               to: email,    //"pranay kadu <kpranay@gmail.com>", // comma separated list of receivers
               subject: "Password of Vegetablebazar", // Subject line
               html: tempHTML // plaintext body
            }, function(error, response){
               if(error){
                   console.log(error);
               }else{
                   console.log("Message sent: " + response.message);
               }
            });


}
 /*generatePdf(inputData, function (err, doc) {
        if (err) return callback(err);

        var bufferChunks = [];

        doc.on('readable', function() {
            // Store buffer chunk to array
            bufferChunks.push(doc.read());
        });
        doc.on('end', function() {

            var pdfBuffer = Buffer.concat(bufferChunks),
                pdfBase64String = pdfBuffer.toString('base64');

            // This string is perfectly ok to use as an attachment to the mandrillAPI
            //pdfBase64String;
        });
    });*/


var sendEmail =function(orderData){
//////////////for email testing///////////////////////

            var allitems = JSON.parse(orderData.order_detail);
            var customer= JSON.parse(orderData.customer_details);
          console
            var tempHTML ="Hi " +   customer.name +",<br/><br/> Thank you for your order, we will deliver your order tomorrow between <br/><br/> "
            tempHTML=tempHTML + orderData.delivery_time +"<br/><br/> Order Details :"
            tempHTML=tempHTML+ '<table><tr><td>name</td>Quantity<td>Price</td><td>Total</td></tr>';
            for(var i=0;i<allitems.length;i++){
            tempHTML= tempHTML+ '<tr>';
            tempHTML= tempHTML+ '<td>' + allitems[i].name + '</td>' ;
            tempHTML= tempHTML+ '<td>' + allitems[i].quantity + ' kg</td>' ;
            tempHTML= tempHTML+ '<td>' + allitems[i].price + ' Rs/kg</td>';
             tempHTML= tempHTML+ '<td>' + allitems[i].quantity * allitems[i].price + ' Rs</td>' ;
             tempHTML= tempHTML+ '</tr>';
           }
            tempHTML= tempHTML+'</table>';
            tempHTML= tempHTML+ "<br> Pay <strong>Rs " +  orderData.total + " </strong> <br><br>";
            tempHTML= tempHTML+ " <h2>DairynBakeryBazar </h2><br> Drink Milk Everyday, Be Stroger, GrowStronger.<br> we hope to see you again";
             tempHTML= tempHTML+ "For any queries/complain: <br>";
             tempHTML= tempHTML+ "mail us :- support@vegetablebazar.com <br>";


            console.log(tempHTML);
            var nodemailer = require("nodemailer");

            var smtpTransport = nodemailer.createTransport("SMTP",{
               service: "Gmail",
               auth: {
                   user: "dnbbazar@gmail.com",
                   pass: "Company@2016"
               }
            });

            smtpTransport.sendMail({
               from: "DairynBakeryBazar <dnbbazar@gmail.com>", // sender address
               to: customer.email,    //"pranay kadu <kpranay@gmail.com>", // comma separated list of receivers
               subject: "Order Confirmation", // Subject line
               html: tempHTML // plaintext body
            }, function(error, response){
               if(error){
                   console.log(error);
               }else{
                   console.log("Message sent: " + response.message);
               }
            });

};

app.get('/getCustomerDetails',function(req,res)
{
  var number=req.query.number;
  var tm =[number];
  console.log(req.query);
  connection.query("SELECT cr.id,cr.name,cr.mobile,cr.email,cr.address,cr.state,cr.master_city,cr.city,cr.area,cr.status,cr.password,cr.mobile_auth_key,cr.isVerified,cr.device_token,cr.payment_method, c.name cityname,c.id cityid, a.id areaid FROM tbl_customer_registration cr join tbl_city c on c.id=cr.city join tbl_area a on a.id=cr.area where cr.mobile = ?",tm,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/getOrderId',function(req,res)
{
  var number=req.query.number;
  var tm =[number];
  console.log(req.query);
  connection.query("SELECT id,total FROM tbl_order where status = 'pending' and mobile = ?",tm,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/getOrderDetails',function(req,res)
{
  var number=req.query.number;
  var tm =[number];
  console.log(req.query);
  connection.query("SELECT * FROM tbl_order where id = ?",tm,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/getOfferDetails',function(req,res){
   var AuthData =JSON.parse(req.get('data'));
  


if (AuthData.city){

               // var AuthData =JSON.parse(req.get('data'));
                       var city=AuthData.city;
                        console.log(AuthData);
                      connection.query("Select id from tbl_city where name = ? ",city ,function(err,rows){
                         if(err)
                          {
                            console.log("Problem with MySQL"+err);
                          }
                          else
                            {
                              var cityid=rows[0].id;
                              var test=[];
                               test.push([AuthData.offer]);
                                test.push([cityid]);
                                console.log(JSON.stringify(test));
                              connection.query("CALL sp_get_offer_details_citywise(?,?) ",test ,function(err,rows){
                                 if(err)
                                  {
                                    console.log("Problem with MySQL"+err);
                                  }
                                  else
                                    {
                                      console.log("offers"+JSON.stringify(rows[0]));
                                      res.end(JSON.stringify(rows[0]));
                                    }
                              });
                            }
                      });

}
else{

     var test=[];
     test.push([AuthData.offer]);
     test.push([AuthData.mobile]);
     console.log(AuthData);
     connection.query("CALL sp_get_offer_details(?,?)",test ,function(err,rows){
         if(err)
          {
            console.log("Problem with MySQL"+err);
          }
          else
            {
              console.log("offers"+JSON.stringify(rows[0]));
              res.end(JSON.stringify(rows[0]));
            }
      });
}


 
});
app.get('/GetSupplierPrice',function(req,res)
{
  var data =JSON.parse(req.query.data);
  var city=data.city;
  var temp=[]; 
  temp.push(city);
  temp.push(city);
  temp.push(city);
  temp.push(city);
 console.log("================"+temp+"===============");
  connection.query("(Select bakery_id id,name,supplier_price,todays_price,date,city,status from tbl_bakery_updated_rate where city like ? and date = (select date from tbl_bakery_updated_rate where city like ? and status='approved' ORDER BY date DESC LIMIT 1)) UNION (Select dairy_id id,name,supplier_price,todays_price,date,city,status from tbl_dairy_updated_rate where city like ? and date = (select date from tbl_dairy_updated_rate where city like ? and status='approved' ORDER BY date DESC LIMIT 1))",temp,function(err,rows)
  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/resetpassword',function(req,res)
{
  var data =JSON.parse(req.get('data'));
  var temp=[];

  var password=data.newpassword;
  var mobile=data.mobile;

  temp.push(password);
  temp.push(mobile);
  console.log(temp);
  connection.query("update tbl_customer_registration SET  password= ? where mobile =?",temp,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/updateOrder',function(req,res)
{
  var data =JSON.parse(req.query.data);
  var temp=[];

  var order_detail=data.order_detail;
  var coupon_code=data.coupon_code;
  var total_before_coupon_apply=data.total_before_coupon_apply;
  var total=data.total;
  var id=data.id;

  temp.push(order_detail);
  temp.push(coupon_code);
  temp.push(total_before_coupon_apply);
  temp.push(total);
  temp.push(id);
  console.log(temp);
  connection.query("update tbl_order SET  order_detail=?,coupon_code=?,total_before_coupon_apply=?,total=? where id =?",temp,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/getVendorePaymentDetails',function(req,res)
{
  var data =JSON.parse(req.get('data'));
  var temp=[];
  temp.push(data);
  connection.query("Select s.price,s.city,c.name as cityname from tbl_subscription_order s join tbl_city c on c.id=s.city where DATE_FORMAT(s.date, '%Y-%m-%d') = ? and s.status='Delivered'",temp,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/getSubscriptionOrderDetails',function(req,res)
{
  var data =JSON.parse(req.get('data'));
  var temp=[];

  var date=data.date;
  var city=data.city;

  temp.push(city);
  temp.push(date);
  connection.query("Select * from tbl_subscription_order where city = ? and DATE_FORMAT(date, '%Y-%m-%d') = ? and status='Delivered'",temp,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/getRechargeDetails',function(req,res)
{
  var data =JSON.parse(req.get('data'));
  var temp=[];

  var date=data.date;
  var city=data.city;

  temp.push(city);
  temp.push(date);
  console.log(temp);
  connection.query("Select rm.*, cr.name, cr.city from tbl_recharge_master rm join tbl_customer_registration cr on rm.mobile = cr.mobile where cr.city = ? and DATE_FORMAT(rm.date, '%Y-%m-%d')=?",temp,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/getDeliveryTime',function(req,res){
  //var number=req.query.number;
  //var tm =[number];
  var varUserid =req.query.number;
  var query;
 //query=  " SELECT a.id, dt.name, a.name FROM tbl_area_delivery_time ad INNER JOIN tbl_area a ON a.id = ad.area_id INNER JOIN tbl_delivery_time dt ON dt.id = ad.delivery_time_id WHERE a.city_id=1"
  if(varUserid){
       query=  "SELECT a.id, dt.name  FROM tbl_area_delivery_time ad INNER JOIN tbl_area a ON a.id = ad.area_id INNER JOIN tbl_delivery_time dt ON dt.id = ad.delivery_time_id inner join tbl_customer_registration cust on cust.city =a.city_id WHERE cust.mobile=?"
    }
  else{
     query=  "SELECT *  FROM tbl_delivery_time"
  }
  connection.query(query,varUserid,function(err,rows){
   
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
        {
          res.end(JSON.stringify(rows));
        }
  });
});
app.get('/getBalanceDetails',function(req,res)
{
  var data =JSON.parse(req.get('data'));
  var temp=[];
  var city=data.city;

  temp.push(city);
  console.log(temp);
  connection.query("Select b.*, cr.name, cr.city from tbl_balance b join tbl_customer_registration cr on b.mobile = cr.mobile where cr.city = ?",temp,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });
});
app.get('/getCustomerNumberDetails',function(req,res)
{
  var data =JSON.parse(req.get('data'));
  var temp=[];
  var city=data.city;

  temp.push(city);
  console.log(temp);
  connection.query("Select name,mobile,email,city,device_token from tbl_customer_registration where city=? and device_token IS NOT NULL",temp,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/updatecustomerdetail',function(req,res)
{
  var data =JSON.parse(req.get('data'));
  var newdetails = {} ;
  var temp=[];
  console.log(data);

  newdetails.email=data.email;
  newdetails.address=data.address;
  newdetails.city=data.cityid;
  newdetails.area=data.areaid;

  var mobile=data.mobile;

  temp.push(newdetails);
  temp.push(mobile);
  console.log(temp);

  
  connection.query("update tbl_customer_registration SET  ? where mobile =?",temp,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/getPendingSubOrders',function(req,res)
{
  var city=req.query.city;
  var tm =[city];
  connection.query("SELECT s.id,s.customer_name,s.customer_address,s.customer_mobile,s.delivery_time, ct.name as cityname FROM tbl_subscription_order s INNER JOIN tbl_city ct  ON s.city = ct.id WHERE s.status = 'pending' AND STR_TO_DATE(s.date, '%Y-%m-%d') >= DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY) AND STR_TO_DATE(s.date, '%Y-%m-%d') <= DATE_ADD(CURRENT_DATE(), INTERVAL 1 DAY) and s.id not in (select subscription_order_id from tbl_subscriptionorder_n_deliveryboy) and ct.name like ?",tm,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        res.end(JSON.stringify(rows));
      }
  });

});
app.get('/getPendingOrders',function(req,res)
{
  var city=req.query.city;
  var tm =[city];
  connection.query("SELECT o.*, ct.name AS cityname FROM tbl_order o INNER JOIN `tbl_customer_registration` cr  ON cr.mobile = o.mobile INNER JOIN `tbl_city` ct  ON cr.city = ct.id WHERE o.status = 'pending' AND STR_TO_DATE(o.date, '%Y-%m-%d') >= DATE_SUB(CURRENT_DATE(), INTERVAL 1 DAY) AND STR_TO_DATE(o.date, '%Y-%m-%d') <= DATE_ADD(CURRENT_DATE(), INTERVAL 1 DAY) and o.id not in (select order_id from tbl_order_n_deliveryboy) and ct.name = ?",tm,function(err,rows)

  {
    if(err)
      {
        console.log("Problem with MySQL"+err);
      }
      else
      {
        console.log(JSON.stringify(rows));
        res.end(JSON.stringify(rows));
      }
  });

});

/*
/*
  * Start the Express Web Server.
*/
app.listen(10000,function(){
  console.log("It's Started on PORT 10000");
});
