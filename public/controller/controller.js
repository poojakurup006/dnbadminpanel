//credentialsController
app.controller('credentialsController',['$scope','$location','AuthenticationService','$routeParams',function($scope,$location,AuthenticationService,$routeParams){
//var id=$routeParams.id;
$scope.staffinfo={};
$scope.saveCredentials = function(){ 
  
  
            AuthenticationService.SaveCredentials( $scope.staffinfo.username, $scope.staffinfo.password, $routeParams.id).then( function (response) {
                
                    $location.path('/feature/'+response.insertId);
                
            },function(){})
        };
}]);

app.controller('headerCtrl',['$scope',function($scope){
   $scope.check = function (id) {
    $scope.menuData=JSON.parse(window.localStorage.testMenudata);

        var ifExist = false;
        angular.forEach($scope.menuData, function (data) {

            if (data.menu_id == id)

            ifExist = true;



        });
        return ifExist;

    }
}]);
app.controller('showStaffcontroller',['$scope','StaffService',function($scope,StaffService){
	

          var self = this;
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.allStaff=[];

               
          self.fetchAllStaff = function(){
              StaffService.fetchAllStaff()
                  .then(
                               function(d) {
                                    self.allStaff = d;
                                    //alert(angular.toJson(self.allStaff));
                                    var s = angular.copy(d);
                                    for (var i = s.length - 1; i >= 0; i--) {
                                                                       
                                    delete s[i].ID;
                                    delete s[i].image_path;
                                    delete s[i].comment;
                                    };
                                    //alert(angular.toJson(s));
                                    $scope.getArray= s;
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };
          self.fetchAllStaff();
          $scope.getHeader = function () {return ["Staff Name","Email Id","Mobile No","Local Address","City","State","Zip Code","Residential Address","Landline No","address Proof","Id Proof","assigned_branch","assigned_designation"]};
}]);

app.controller('leadFollowupcontroller',['$scope','$rootScope','$routeParams','LeadService','$filter','$http','$location', '$timeout','ngDialog' ,function($scope,$rootScope,$routeParams,LeadService,$filter,$http,$location,$timeout,ngDialog){
	   
     var today=new Date().setHours(0,0,0,0);
      $scope.today = new Date(today);
		 var leadId =  $rootScope.leadId;

     var home =  $rootScope.home;
      $scope.next_followup_date = $filter("date")(Date.now(), 'yyyy-MM-dd');

     //alert("test");

		$scope.isCollapsed=true;
          var self = this;
         
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
          self.allFollowups=[];
          //self.allFollowups=test;
          self.newFollowup={lead_id :leadId };

          var y = new Date();
          self.newFollowup.next_followup_time=y.getHours( )+ ":" +  y.getMinutes();

            //$scope.a=10;   
          self.fetchAllFollowups = function(leadId){
              LeadService.fetchAllFollowups(leadId)
                  .then(
                               function(d) {
                                    self.allFollowups = d;
                                    $timeout(function(){
                                      $scope.$apply(function(){self.allFollowups = d;});},0);
                                    
                                    var p = self.allFollowups
                                    for (var i = p.length - 1; i >= 0; i--) {
                                    delete p[i].ID;
                                    delete p[i].staff_id;
                                    delete p[i].lead_id;
                                    };
                                    //alert(angular.toJson(p));
                                    $scope.getArray= p;
                                    //alert(angular.toJson(self.allFollowups));
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };
          $scope.updateleads= function(data){
             var id=  $routeParams.leadId;
             var status= data;
            var alldata=[];
            alldata.push(status);
            alldata.push(id);
            
           /* alert(angular.toJson(alldata));*/
            var req={
            method: 'POST',
            url: '/updatelead',
            headers: {
                  'Content-Type': 'application/json', data:angular.toJson(alldata)
             }};
             $http(req).then(
                                function(d) {
                                  
                                  }, function(errResponse){
                                      console.error('Error while insertng updating');
                                                      }
                                             );
                             $location.path('/showallleads'); 

          };
          self.addFollowup = function(){ 
          		 self.newFollowup.next_followup_date=moment(self.newFollowup.next_followup_date.toString()).format("DD/MM/YYYY");
               self.newFollowup.next_followup_time=moment(self.newFollowup.next_followup_time.toString()).format("HH:mm A");


		   /*self.newFollowup.next_followup_date =new Date(tempdate.getFullYear() + "-" + ( tempdate.getMonth() >9 ? (tempdate.getMonth()+1) : "0" + (tempdate.getMonth() +1) )+ "-"  + (tempdate.getDate() >=10 ? tempdate.getDate() : "0" + tempdate.getDate()));*/





              LeadService.addFollowup(self.newFollowup)
                  .then(
                               function(d) {

                                     d;
                                     self.newFollowup={};
                                     self.fetchAllFollowups(leadId);
                                     self.newFollowup={lead_id :leadId };
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies'+(angular.toJson(errResponse)));
                                }

                       );
          };
           
          
           if($rootScope.source=="current")
           {     
              //self.leadDetails=$rootScope.followupleads[0];
             // alert(leadId);
              self.leadDetails=LeadService.fetchLeadSpecificData(leadId, $rootScope.followupleads);
              //alert(angular.toJson(self.leadDetails)); 

           }
           else if($rootScope.source=="pending")
            {  
              //self.leadDetails=$rootScope.pendingleads[0];
              //alert(angular.toJson($rootScope.pendingleads)); 
              self.leadDetails=LeadService.fetchLeadSpecificData(leadId,angular.fromJson(sessionStorage.pendingOrders));
               //alert(angular.toJson(self.leadDetails)); 

           }
           else{
            self.leadDetails=LeadService.fetchLeadSpecificData(leadId,angular.fromJson(sessionStorage.GetAllLeads));
            
}
          self.customer=JSON.parse(self.leadDetails.customer_details);
          self.order=JSON.parse(self.leadDetails.order_detail);
          
         // self.fetchAllFollowups(leadId);
          $scope.getHeader = function () {return ["Remark","Date","Next Followup Date"]};
          

}]);


app.controller('homeController',['$rootScope','$scope', '$http', '$q', '$filter', '$timeout', '$location', 'ngDialog','LeadService','CartService','Excel' ,function($rootScope,$scope, $http, $q, $filter, $timeout, $location,ngDialog,LeadService,CartService,Excel)
  {
$timeout(function(){
  $('.table').trigger('footable_redraw');
}, 1000);
$scope.date = new Date();
$scope.exportToExcel=function(tableId){ // ex: '#my-table'
            var exportHref=Excel.tableToExcel(tableId,'sheet name');
            $timeout(function(){location.href=exportHref;},100); // trigger download
        };

    GetRoleWiseCityName=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/GetRoleWiseCityName/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    };
    GetRoleWiseCityName();
   GetAllLeadDetails= function() {
    
                    $http({
    url: '/GetAllLeadDetails', 
    method: "GET",
    params: {city: $scope.selectedcity}
 }).then(
                                    function(response){
                                         //$rootScope.source="current";
                                          $scope.followuplead= response.data;
                                        $rootScope.followupleads= response.data;
                                       //alert(angular.toJson($rootScope.followupleads));
                                        var test= new Date();
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
            };
  getPendingOrders = function(){
    $http({
    url: '/getPendingOrders', 
    method: "GET",
     params: {city: $scope.selectedcity}
 }).then(
            function(response){
              $rootScope.source="pending";
              $scope.allproducts=[];
              sessionStorage.pendingOrders = angular.toJson(response.data);
              $scope.pendingOrders = response.data;
              $scope.data=getAllProducts(response.data);
              var alldata={"city":$scope.pendingOrders[0].cityname};
              CartService.getSupplierPrice(alldata,$scope.data)
              .then(function(data){
                $scope.allproducts=data;
                }, function(err) { 
                });
              
            },
            function(errResponse){
              console.error("Error while fetching pending orders");
            }
            );
  };

  function getAllProducts(pendingOrders){
var result = [];
var orderArray;

for(let i=0;i< pendingOrders.length;i++ ){
  orderArray=null;
   orderArray=JSON.parse(pendingOrders[i].order_detail);
result.forEach( function( entry ) {
    for( let j = 0; j < orderArray.length; ++j ) {
        if( entry.id === orderArray[j].id && entry.name === orderArray[j].name) {
            entry.quantity=entry.quantity+ orderArray[j].quantity;
            orderArray.splice( j, 1 );
            return;
        }
    }
   
} );
orderArray.forEach( function( entry ) {
    result.push(entry);
} );

}
//alert(angular.toJson(result));
return result;
}; //function end



  $scope.showName=function(data){
    var cust=JSON.parse(data);
    console.log(cust.name);
    return cust.name;

  };
   $scope.showMobile=function(data){
    return ( JSON.parse(data).address + " \n"+JSON.parse(data).mobile);
   

  };
  $scope.showOrder=function(d){
    
       var orderdata= LeadService.fetchLeadSpecificData(d,JSON.parse(sessionStorage.pendingOrders))  ; 
   
console.log( "oraderdata=======>"+angular.toJson(orderdata));
   return (JSON.parse(orderdata.order_detail));
   

  };

  
  $scope.change=function(d){
    $rootScope.followuplead=[];
    $scope.allproducts=[];
    $scope.pendingOrders =[];
    GetAllLeadDetails();
    getPendingOrders();

  };
    

            $scope.showDetails = function(id,showpending){
                  
                   
                   $rootScope.leadId=id;
                   //alert("inn showDetails" + id);
                   $rootScope.home="home";
                    $rootScope.source=showpending;
                     
                    //$scope.T_Url = views/viewlead/view_lead.html;
                      ngDialog.open({template: 'views/viewlead/view_lead.html', controller:'leadFollowupcontroller as followup', scope: $scope});
                };

                 


  }]);

app.controller('ForgetPasswordController',['$rootScope','$scope', '$http', '$q', '$filter', '$timeout', '$location', 'ngDialog','AuthService',function($rootScope,$scope, $http, $q, $filter, $timeout, $location,ngDialog,AuthService)
{
$scope.recoverPasswordonMobile = function(mobile){

    AuthService.recoverPasswordonMobile(angular.toJson(mobile))
      .then(function(res){
        if (res && res.length>0 ) {
          ngDialog.openConfirm({template: 'Password has been sent to your Mobile ',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                                }).then(function (success) {
                                  }, function (error) {
                                  });
                          $location.path('/login');
        }
        else{
          ngDialog.openConfirm({template: 'Mobile does not exists in our records',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });}
         
      }, function(err) { 
        ngDialog.openConfirm({template: 'There is an error.. Please try agai',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });
    });
  };


 }]);

app.controller('vendorSubscriptionController',['$rootScope','$scope', '$http', '$q', '$filter', '$timeout', '$location', 'ngDialog','$window','LeadService','CartService','Excel' ,function($rootScope,$scope, $http, $q, $filter, $timeout, $location,ngDialog,$window,LeadService,CartService,Excel){
$scope.exportToExcel=function(tableId){ // ex: '#my-table'
            var exportHref=Excel.tableToExcel(tableId,'sheet name');
            $timeout(function(){location.href=exportHref;},100); // trigger download
        };
$scope.GetVendorPayment = function(date) {
  $http({
      url: '/getVendorePaymentDetails', 
      method: "GET",
      headers: {'Content-Type': 'application/json','data':angular.toJson(date)}
         }).then(
                    function(response){
                      $scope.subdetails = response.data;
                        if ($scope.subdetails.length === 0) {
                          ngDialog.openConfirm({template: 'Soory... No data available for this date',
                                  appendClassName: 'ngdialog-theme-default1',
                                  plain: true
                            }).then(function (success) {
                              $window.location.reload();
                              }, function (error) {
                                $window.location.reload();
                              });
                        } else{
                          $scope.product=getAllProducts(response.data);
                        }           
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      ngDialog.openConfirm({template: 'Error while fetching customer details',
                                  appendClassName: 'ngdialog-theme-default1',
                                  plain: true
                            }).then(function (success) {
                              $window.location.reload();
                              }, function (error) {$window.location.reload();
                              });
                    });

};
function getAllProducts(suborder){
var result = [];
var orderArray;

for(let i=0;i<suborder.length;i++ ){
  orderArray=null;
   orderArray=suborder;
    for( let j = i+1; j < orderArray.length; j++ ) {

        if( suborder[i].city === orderArray[j].city) {
            suborder[i].price=suborder[i].price+ orderArray[j].price;
            orderArray.splice( j, 1 );
        }
    }
}
return orderArray;
};

}]);
app.controller('featureController',['$scope','$routeParams','featureService','$http','$location',function($scope,$routeParams,featureService,$http,$location){
  

          var self = this;
         // self.Staffvisitor_name:'',contact_no:'',lead_type:'',max_rent:'',location:'',pro_type:''};
         $scope.selectedSubMenus=[];
        $scope.allFeature=[];

         $scope.showMe = false;
          $scope.myFunc = function() {
              $scope.showMe = !$scope.showMe;
          }
               
          self.fetchAllFeature = function(){
              featureService.fetchAllFeature()
                  .then(
                               function(d) {
                                    $scope.allFeature = d;
                                  
                                    //alert(angular.toJson($scope.allFeature));
                                    
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };
          self.fetchMainMenu = function(){
              featureService.fetchMainMenu()
                  .then(
                               function(d) {
                                    $scope.mainMenu = d;
                                  
                                    //alert(angular.toJson($scope.mainMenu));
                                    
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };

          /**/
          $scope.staffinfo={};
          $scope.addFeature = function(){
           
            var subfeature=$scope.selectedSubMenus.filter(function(val){return val !== null;});
            var alldata={"staffid": $routeParams.id,"selectedSubMenus":subfeature};
                var req={
              method: 'POST',
              url: '/InsertFeature',
              headers: { 'Content-Type': 'application/json', data:angular.toJson(alldata)
               }};
                    
                $http(req).then(
                                           function(d) {
                                                $scope.subfeature={};
                                                //alert(angular.toJson(subfeature));
                                           },
                                            function(errResponse){
                                                console.error('Error while insertng InsertFeature');
                                            }
                                   );
                   $location.path('/staff');      
            },
          self.fetchAllFeature();
          self.fetchMainMenu();
        
}]);
app.controller('gcmNoticeController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams',function($rootScope,$filter,$scope,$http,$timeout,$routeParams){
 
 GetRoleWiseCityName=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/GetRoleWiseCityName/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );
    }
    GetRoleWiseCityName();

    $scope.search=function(city){
    var data={'city':city};
    $http({
      url: '/getCustomerNumberDetails', 
      method: "GET",
      headers: {'Content-Type': 'application/json','data':angular.toJson(data)}
         }).then(
                    function(response){
                      $scope.gcmnotice = response.data;
                         //alert(angular.toJson($scope.gcmnotice)); 
                      if ($scope.gcmnotice.length === 0) {
                          alert("Soory... No data available for this city");
                        }         
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      alert("Soory... No data available for this city");
                    }
                    );
       };
       
  $scope.selectAll = function(data) {
          angular.forEach($scope.gcmnotice, function(ab) {
            ab.Selected  = data;
          });
        };
        $scope.sendnotice=function(data){
          $scope.devicetoken=[];
          angular.forEach($scope.gcmnotice, function(value){
            if (value.Selected) {
              $scope.devicetoken.push(value.device_token);
            }           
          });
            alert(angular.toJson($scope.devicetoken));
            alert(data);
  };

        

}]);
app.controller('deliveredSubscriptionController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog){
GetRoleWiseCityName=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/GetRoleWiseCityName/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );
    }
    GetRoleWiseCityName();
    $scope.search=function(city,date){
      var data={'city':city,'date':date};
    $http({
      url: '/getSubscriptionOrderDetails', 
      method: "GET",
      headers: {'Content-Type': 'application/json','data':angular.toJson(data)}
         }).then(
                    function(response){
                      $scope.subdetails = response.data;
                      var sumofAll=0; 
                        for(i=0;i<$scope.subdetails.length;i++){
                            sumofAll=sumofAll + $scope.subdetails[i].price;
                        }  
                        $scope.sum= sumofAll; 
                        if ($scope.subdetails.length === 0) {
                          ngDialog.openConfirm({template: 'Soory... No data available for this city and date',
                                  appendClassName: 'ngdialog-theme-default1',
                                  plain: true
                            }).then(function (success) {
                              }, function (error) {
                              });
                        }            
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      ngDialog.openConfirm({template: 'Error while fetching customer details',
                                  appendClassName: 'ngdialog-theme-default1',
                                  plain: true
                            }).then(function (success) {
                              }, function (error) {
                              });
                    });
  };

}]); 
app.controller('rechrgeController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams',function($rootScope,$filter,$scope,$http,$timeout,$routeParams){
GetRoleWiseCityName=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/GetRoleWiseCityName/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );
    }
    GetRoleWiseCityName();
    $scope.search=function(city,date){
      $scope.date=$filter('date')(date, "yyyy-MM-dd");
      var data={'city':city,'date':$scope.date};
    $http({
      url: '/getRechargeDetails', 
      method: "GET",
      headers: {'Content-Type': 'application/json','data':angular.toJson(data)}
         }).then(
                    function(response){
                      $scope.rehrgedetails = response.data;
                      var sumofAll=0; 
                        for(i=0;i<$scope.rehrgedetails.length;i++){
                            sumofAll=sumofAll + $scope.rehrgedetails[i].amount;
                        }  
                        $scope.sum= sumofAll; 
                        if ($scope.rehrgedetails.length === 0) {
                          alert("Soory... No data available for this city and date");
                        }            
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      alert("Soory... No data available for this city and date");
                    }
                    );
  };

}]);

app.controller('balanceController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams',function($rootScope,$filter,$scope,$http,$timeout,$routeParams){
GetRoleWiseCityName=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/GetRoleWiseCityName/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );
    }
    GetRoleWiseCityName();
    $scope.search=function(city){
      var data={'city':city};
    $http({
      url: '/getBalanceDetails', 
      method: "GET",
      headers: {'Content-Type': 'application/json','data':angular.toJson(data)}
         }).then(
                    function(response){
                      $scope.balancedetails = response.data;
                      var sumofAll=0; 
                        for(i=0;i<$scope.balancedetails.length;i++){
                            sumofAll=sumofAll + $scope.balancedetails[i].balance;
                        }  
                        $scope.sum= sumofAll;
                      if ($scope.balancedetails.length === 0) {
                          alert("Soory... No data available for this city");
                        }              
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      alert("Soory... No data available for this city and date");
                    }
                    );

  };
}]);

app.controller('deliverytimeController',['$rootScope','$filter','$scope','ngDialog','$http','$timeout','$routeParams','$window',function($rootScope,$filter,$scope,ngDialog,$http,$timeout,$routeParams,$window){
GetAllDeliveryTime=function()
    {
      $http.get('/getAllDeliveryTime')
                            .then(
                                    function(response){
                                        $scope.deliveryData= response.data;
                                        $scope.selecteddiltime=$scope.deliveryData[0];

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
}
GetAllDeliveryTime();
GetAllCityName=function()
    {
      $http.get('/GetCityName')
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                        $scope.selectedCity= $scope.cityName[0];

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetAllCityName();
    $scope.citychange=function(cityId)
      {
        $http({
            url:'/GetAreaName',
            method: "GET",
            params: {city: cityId}
                }).then(
                                    function(response){
                                        $scope.areaName= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
       };

      $scope.selectAll = function(data) {
          angular.forEach($scope.areaName, function(ab) {
            ab.Selected  = data;
          });
        };

    $scope.addDelTime=function(data){

      //alert(angular.toJson(data));
      var req={method: 'GET', url:'/AddDeliveryTime',
                         headers: {'Content-Type': 'application/json','data':angular.toJson(data)}
                  };
              $http(req).success(function (res) {
                        GetAllDeliveryTime();
                        ngDialog.openConfirm({template: 'Successfully Inserted Delivery Time',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });
                    }).error( function(data){
                          console.log(angular.toJson(data));
                          alert("Sorry Fail to Update try again later");
                      });
      };
      $scope.addareadeltime=function(time){
        $scope.time=time;
        //alert(angular.toJson($scope.time));
        var count=0;

        $scope.delarea=[];
          angular.forEach($scope.areaName, function(value){
            if (value.Selected) {
              $scope.delarea.push({'delivery_time_id': time,'area_id':value.id});
              count=count+1;
            } 
          });
          //alert(angular.toJson($scope.delarea));
          if(count>0)
          {
           var req={method: 'GET', url:'/AddAreaDeliveryTime',
                         headers: {'Content-Type': 'application/json','data':angular.toJson($scope.delarea)}
                  };
              $http(req).success(function (res) {
                        ngDialog.openConfirm({template: 'Successfully Inserted Area Wise Delivery Time',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });
                    }).error( function(data){
                          console.log(angular.toJson(data));
                          alert("Sorry Fail to Update try again later");
                      });
          }
          else
          {
             ngDialog.openConfirm({template: 'Please select area',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                      }, function (error) {
                      });
          }


      };

}]);

app.controller('addSCAController',['$rootScope','$filter','$scope','ngDialog','$http','$timeout','$routeParams','$window','_',function($rootScope,$filter,$scope,ngDialog,$http,$timeout,$routeParams,$window,_){
GetAllState=function()
    {
      $http.get('/GetAllState')
                            .then(
                                    function(response){
                                        $scope.stateData= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetAllState();

    GetAllMasterCity=function()
    {
      $http.get('/GetAllMasterCity')
                            .then(
                                    function(response){
                                        $scope.masterCityData= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetAllMasterCity();

    GetAllCityName=function()
    {
      $http.get('/GetCityName')
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetAllCityName();

    $scope.citychange=function(cityId)
      {
        $http({
            url:'/GetAreaName',
            method: "GET",
            params: {city: cityId}
                }).then(
                                    function(response){
                                        $scope.areaName= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
       };

    $scope.addState=function(data){

      var req={method: 'GET', url:'/addNewState',
                         headers: {'Content-Type': 'application/json','data':angular.toJson(data)}
                  };
              $http(req).success(function (res) {
                        GetAllState();
                        ngDialog.openConfirm({template: 'Successfully Inserted State',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });
                    }).error( function(data){
                          console.log(angular.toJson(data));
                          alert("Sorry Fail to Update try again later");
                      });
      };
      $scope.addMasterCity=function(sid,mastercity){

        $scope.data={'name':mastercity,'state_id':sid}

      var req={method: 'GET', url:'/addNewMasterCity',
                         headers: {'Content-Type': 'application/json','data':angular.toJson($scope.data)}
                  };
              $http(req).success(function (res) {
                        GetAllCityName();
                        ngDialog.openConfirm({template: 'Successfully Inserted Master City',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });
                    }).error( function(data){
                          console.log(angular.toJson(data));
                          alert("Sorry Fail to Update try again later");
                      });
      };
      $scope.addCity=function(mcid,city){
        $scope.mc=_.where( $scope.masterCityData, { id:parseInt(mcid)} );

        $scope.data={'name':city,'state_id':$scope.mc[0].state_id,'master_city_id':$scope.mc[0].id};

      var req={method: 'GET', url:'/addNewCity',
                         headers: {'Content-Type': 'application/json','data':angular.toJson($scope.data)}
                  };
              $http(req).success(function (res) {
                        GetAllCityName();
                        ngDialog.openConfirm({template: 'Successfully Inserted City',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });
                    }).error( function(data){
                          console.log(angular.toJson(data));
                          alert("Sorry Fail to Update try again later");
                      });
      };
      $scope.addArea=function(cid,area){
        $scope.mc=_.where( $scope.cityName, { id:parseInt(cid)} );
        $scope.areadata={'name':area,'city_id':$scope.mc[0].id,'state_id':$scope.mc[0].state_id}

      var req={method: 'GET', url:'/addNewArea',
                         headers: {'Content-Type': 'application/json','data':angular.toJson($scope.areadata)}
                  };
              $http(req).success(function (res) {
                        ngDialog.openConfirm({template: 'Successfully Inserted Area',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });
                    }).error( function(data){
                          console.log(angular.toJson(data));
                          alert("Sorry Fail to Update try again later");
                      });
      };
}]);

app.controller('productCtrl',['$rootScope','$filter','$scope','ngDialog','$ionicPopup','$http','$timeout','$routeParams','$window','CartService','_',function($rootScope,$filter,$scope,ngDialog,$ionicPopup,$http,$timeout,$routeParams,$window,CartService,_){
 $scope.vegetables=!_.isUndefined(window.localStorage.lsallVegetables) ?
                            JSON.parse(window.localStorage.lsallVegetables) : [];

$scope.fruits=!_.isUndefined(window.localStorage.lsallFruits) ?
                            JSON.parse(window.localStorage.lsallFruits) : [];


$scope.VegetableDetails=function(){
    var req={
               method: 'GET',
               url:'/getVegetables',
               headers: {
                         'Content-Type': 'application/json'
                                      
                         }
             };
    $http(req).then(
                               function(d) {
                                window.localStorage.lsallVegetables=JSON.stringify(d.data);
                                    $scope.vegetables=d.data;
                                    CartService.getListPrices('/getVegetablePrices',$scope.vegetables)
                                    .then(function(res){$scope.vegetables=res;},
                                      function(err){console.log("There is an error ");}) ;
  
                               },
                                function(errResponse){
                                    console.error('Error while fetching vegetablelist');
                                }
                       );


    $scope.$on('priceChange', function (event, args) {
    CartService.getListPrices('/getVegetablePrices',$scope.vegetables).then(function(res){$scope.vegetables=res;},function(err){console.log("There is an error ");}) ;
    // CartService.CartChange();     
  });

};
$scope.FruitDetails=function(){
    var req={
               method: 'GET',
               url:'/getFruits',
               headers: {
                         'Content-Type': 'application/json'
                                      
                         }
             };
    $http(req).then(
                               function(d) {
                                  window.localStorage.lsallFruits=JSON.stringify(d.data);
                                    $scope.fruits=d.data;
                                    CartService.getListPrices('/getFruitPrices',$scope.fruits)
                                    .then(function(res){  $scope.fruits=res;}
                                      ,function(err){console.log("error occured"+err);});
                               },
                                function(errResponse){
                                    console.error('Error while fetching fruitlist');
                                }
                       );



  $scope.$on('priceChange', function (event, args) {
     $scope.message = args.message;
      CartService.getListPrices('/getFruitPrices',$scope.fruits).then(function(res){ $scope.fruits=res;},function(err){
  console.log("error occured"+err);});
       //$scope.$emit('CartChange', { priceList: $scope.fruits });
 });
};
$scope.cart=[];
$scope.showPopup = function(id,name,hindi_name,image_domain,img,price,quan,unit) {
     $scope.data = {};
     $scope.custom=false;
     $scope.data.id=id;
     $scope.data.name=name;
     $scope.data.hindi_name=hindi_name;
     $scope.data.image_domain=image_domain;
     $scope.data.image=img;
     $scope.data.price=price;
     $scope.data.quan=quan;
     $scope.data.unit=unit;

     if ($scope.data.unit === 1) {
      $scope.quantityList=[{'name':'1','value':1},{'name':'2','value':2},{'name':'3','value':3},{'name':'4','value':4},
             {'name':'5','value':5},{'name':'6','value':6},{'name':'7','value':7},{'name':'8','value':8},{'name':'9','value':9},{'name':'10','value':10}];
     }
     else{
      $scope.quantityList=[{'name':'0.25','value':0.25},{'name':'0.5','value':0.5},{'name':'0.75','value':0.75},{'name':'1','value':1},
                 {'name':'1.5','value':1.5},{'name':'2','value':2},{'name':'2.5','value':2.5},{'name':'3','value':3},{'name':'3.5','value':3.5},{'name':'4','value':4},{'name':'4.5','value':4.5},{'name':'5','value':5}];

     }

    
                 
     var eitem=CartService.checkExistingItem($scope.data);
     if(eitem){$scope.data=eitem; }
     
      // An elaborate, custom popup
      if ($scope.data.unit === 1) {
        var myPopup = $ionicPopup.show({
        template: '<style type="text/css">.item-select {width: 100%;}.item-select select {left: 0;}'+
           '</style><form name="cartvalue"><div ><ion-toggle toggle-class="toggle-energized" ng-model="custom" ng-click="toggleClick($event)" class="borderdesign">Custom Quantity</ion-toggle>'+
               ' <select ng-if="!custom" ng-model="data.quantity" ng-selected="{{quantityList.value==data.quantity}}" ng-options="i.value as i.value for i in quantityList" class="myselectstyle1"><option value="">Select a quantity</option>'+
                '</select>  <input  type="number" name="quantity" min="1" step="1"  ng-pattern="/^[0-9]{1,4}$/" class="myselectstyle1" placeholder="1 Item & above" ng-if="custom" ng-model="data.quantity" style="padding-left:10px;"></input >'+
             '<span> {{data.quan}} </span>'+
             '<span class="invalid" ng-show="cartvalue.quantity.$error.pattern">Please enter valid number</span>'+
             ' </div> </form>'+
                 '<script>var myfunction = function(e) {if(!((e.keyCode > 99 && e.keyCode < 106)|| (e.keyCode > 51 && e.keyCode < 58) || e.keyCode == 8 )) {console.log(e.keyCode);return false;}}</script>',
          cssClass: 'my-custom-popup',
        title: 'Enter Quantity of ' + name,
        scope: $scope,
        buttons: [
          { text: '<b>Cancel</b>',
            type: 'btn-danger', },
          {
            text: '<b>Save</b>',
            type: 'btn-success',
            onTap: function(e) {
              
              
              if (!$scope.data.quantity ) {

                e.preventDefault();

              }
               else {
                
              
                CartService.addCartItem($scope.data);
                console.log(angular.toJson("fgfhgjgukuhjh"+$scope.data));
                $rootScope.$broadcast('CartChange', { message: "test" });

              }
            }
          }
        ]
      });
      }
      else{
        var myPopup = $ionicPopup.show({
        template: '<style type="text/css">.item-select {width: 100%;}.item-select select {left: 0;}'+
               '</style><div ><ion-toggle toggle-class="toggle-energized" ng-model="custom" ng-click="toggleClick($event)">Custom Quantity</ion-toggle>'+
                   ' <select ng-if="!custom" ng-model="data.quantity" ng-selected="{{quantityList.value==data.quantity}}" ng-options="i.value as i.value for i in quantityList" class="myselectstyle"><option value="">Please select a quantity</option>'+
                    '</select> <input  type=number name="qty" min=".25"  class="myselectstyle" placeholder="250gm & above" ng-if="custom" ng-pattern="/\d*\.?\d*/" ng-model="data.quantity"></input >'+
                    '<span> {{data.quan}} </span>'+
                     ' </div> '+
                     '<script>var myfunction = function(e) {if(!((e.keyCode > 99 && e.keyCode < 106)|| (e.keyCode > 51 && e.keyCode < 58) || e.keyCode == 8 )) {console.log(e.keyCode);return false;}}</script>',
        cssClass: 'my-custom-popup',
        title: 'Enter Quantity of ' + name,
        scope: $scope,
        buttons: [
          { text: '<b>Cancel</b>',
            type: 'btn-danger', },
          {
            text: '<b>Save</b>',
            type: 'btn-success',
            onTap: function(e) {
              
              
              if (!$scope.data.quantity ) {

                e.preventDefault();

              }
               else {
                
              
                CartService.addCartItem($scope.data);
                console.log(angular.toJson("fgfhgjgukuhjh"+$scope.data));
                $rootScope.$broadcast('CartChange', { message: "test" });

              }
            }
          }
        ]
      });


      }

      
    };
}]);

app.controller('ResetPasswordController',['$rootScope','$filter','$scope','ngDialog','$http','$timeout','$routeParams','$window',function($rootScope,$filter,$scope,ngDialog,$http,$timeout,$routeParams,$window){

$scope.resetpassword=function(data){

  //alert(angular.toJson(data));
  if(data.newpassword===data.confirmpassword){
  delete data.confirmpassword;

  var req={method: 'GET', url:'/resetpassword',
                     headers: {'Content-Type': 'application/json','data':angular.toJson(data)}
              };
          $http(req).success(function (res) {
                    $scope.reloadRoute = function() {
                       $window.location.reload();
                    };
                    
                    ngDialog.openConfirm({
                    template: 'Successfully updated password',
                    appendClassName: 'ngdialog-theme-default1',
                    plain: true
                    }).then(function (success) {
                          $scope.reloadRoute();
                      }, function (error) {
                          $scope.reloadRoute();
                      });
                    

                }).error( function(data){
                      console.log(angular.toJson(data));
          });
      }

};

}]);

app.controller('editSubscriptionController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$ionicPopup','$ionicLoading','$window','SubscriptionService','AuthService',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$ionicPopup,$ionicLoading,$window,SubscriptionService,AuthService){
$scope.subdata=[];
$scope.pname = [{ id: 0, name: 'Bakery' },{ id: 1, name: 'Dairy' }];
$scope.search=function(phnumber){
  var mobile= angular.toJson(phnumber);
      SubscriptionService.getSubcriptionDetails(mobile).then(function(data){
        window.localStorage.customernumber=phnumber;
        $http({method: 'GET', url:'/getCustomerDetails',params: {number: phnumber}}).then(
          function (res) {
                  var data=res.data;  
                  $scope.customerdetails=data[0];          
                  },function(errResponse){console.log(angular.toJson(errResponse));}); 
        var suballdata=data[0];
        var todaysdate = $filter('date')(new Date(), 'yyyy-MM-d');
        $scope.subdata=[];
        suballdata.forEach(function(entry){
        var LastAvailableDate = entry.end_date;
        if( new Date(todaysdate) <= new Date(LastAvailableDate))
        {
          $scope.subdata.push(entry);
        }
        });
      });
};

$scope.lastDay='';
var lastdate;
var firstdate;
  $scope.showSubDetails=function(subid){
    $scope.milk={};
    $scope.subid=subid;
    DisplaySubDetails();
  };
  DisplaySubDetails = function(){
    $scope.allsubdata=(angular.fromJson($window.localStorage.subscriptiondata));
    $scope.allsubdata.forEach(function(entry)
    {
      if(entry.id==$scope.subid){
        $scope.milk=entry;
        $scope.productType= $scope.milk.pcode===1 ? 'Dairy':'Bakery';

        var between=[];
        var currentDate = new Date($scope.milk.start_date),end = new Date($scope.milk.end_date);
            while (currentDate <= end) {
                between.push(new Date(moment(currentDate).startOf('day')));
                currentDate.setDate(currentDate.getDate() + 1);
            }
            firstdate=new Date($scope.milk.start_date);
            lastdate=new Date($scope.milk.end_date);
            ExceptionDetails(between);
      }
    });
    };

    ExceptionDetails = function(alldates){
      var edata=[];

      var data= angular.toJson($scope.subid);
      var req={method: 'GET',url: '/getException',headers: {'Content-Type': 'application/json',data:data}};
      $http(req).success(function (d) { 

                        window.localStorage.exceptiondata=JSON.stringify(d);
                        console.log("all exception date"+ angular.toJson(d));
                        d.forEach(function(entry){

                           var t= new Date(entry.exception_date).setHours(0,0,0,0);                    

                           alldates.forEach( function(p){ 
                            var test=new Date(entry.exception_date).setHours(0,0,0,0);
                            console.log(test);
                            if(p.getTime() == (test)) {

                              alldates.splice(alldates.indexOf(p),1);
                            }
                           });
                          
                        });
                       calanderDates(alldates);
                        
        }).error( function(data){
          $ionicPopup.alert({
               title: 'Error',
               template:"There is an error.. Please try again " + data});
        });
      
    };
    var todaysdate = $filter('date')(new Date(), 'yyyy-MM-d');
     var td=new Date(todaysdate);
     var ttd = td.setDate(td.getDate() + 1);
     var tomorwdate = $filter('date')(ttd, 'yyyy-MM-d');

    function calanderDates (daterange){
      $scope.myArrayOfDates=[];
      $scope.day_count=daterange.length;
      $scope.total_qty =  $scope.milk.quantity * $scope.milk.frequency * $scope.day_count;
      //alert(angular.toJson(daterange));

      console.log("exception"+angular.toJson(daterange));
      daterange.forEach(function(d){
        
        $scope.myArrayOfDates.push(moment(d).date(d.getDate()).month(d.getMonth()));

      });
       if( firstdate >  new Date(todaysdate))
       {
        $scope.today = (moment(firstdate).date(firstdate.getDate()).month(firstdate.getMonth()));
       }
       else{
          $scope.today = moment();  
       }
      $scope.lastDay = (moment(lastdate).date(lastdate.getDate()).month(lastdate.getMonth()));

    }

    $scope.myArrayOfDates = [moment().date(4)];
    $scope.$watch('myArrayOfDates', function(newValue, oldValue){
        if(newValue){
            console.log('my array changed, new size : ' + newValue.length);
        }
    }, true);


     $scope.selected_dates=[];
     $scope.excpdeldate=[];
     $scope.checkSelection = function(event, date) {
      
        
        var testDate=date.date;
      var test = new Date(testDate);
      var day=test.getDate();
      var month=test.getMonth()+1;
      var year=test.getFullYear();
      var cale_date= year + "-" + month + "-" + day;
      var dateclicked=cale_date;

      if( new Date(dateclicked) >  new Date($scope.milk.end_date) || new Date(dateclicked) < new Date($scope.today)) return;
      if((dateclicked === todaysdate) || (dateclicked === tomorwdate))  return $ionicPopup.alert({title: 'Sorry !!!', template:"You cannot remove Today and Tomorrows subscription ..."});

      if(date.mdp.selected){
      $scope.selected_dates.push(dateclicked);}
      else{
        $scope.selected_dates.forEach(function(d){

          if (d==dateclicked){ 

          $scope.selected_dates.splice($scope.selected_dates.indexOf(d),1);
        }});
      }
      if (!date.mdp.selected) {
        $scope.excpdeldate.push(dateclicked);
      }
      else{
        $scope.excpdeldate.forEach(function(d){

          if (d==dateclicked){ 

          $scope.excpdeldate.splice($scope.excpdeldate.indexOf(d),1);
        }});
      }
      
    };
    $scope.saveexceptiondate = function(){
    $ionicLoading.show({
      template: 'Adding exception date...'
    });
    $scope.arraydate=[];
    $scope.selected_dates.forEach(function(d)
    {
      $scope.arraydate.push(d);
    });
    $scope.excpdeldate.forEach(function(d)
    {
      $scope.arraydate.push(d);
    });
    //$scope.arraydate={activeDate: $scope.selected_dates, deletedDate: $scope.excpdeldate};

    AuthService.saveExceptionDate($scope.arraydate,$scope.milk.id)
      .then(function(data){
        
        //$rootScope.$broadcast('updateSubDateData');
        $scope.search(window.localStorage.customernumber);
        setTimeout(function() {
          $scope.showSubDetails($scope.milk.id);
        }, 1000);
        $scope.selected_dates=[];
        $scope.excpdeldate=[];
        
               $ionicLoading.hide();
        $ionicPopup.alert({
           title: 'Success',
           template:"Exception date added successfully"});
        
        

           
      }, function(err) { 
        
        $ionicLoading.hide();$ionicPopup.alert({
           title: 'Error',
           template:"There is an error.. Please try again " + err});
    });


};
}]);

app.controller('addSubscriptionController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$ionicPopup','$window','CartService','OfferService',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$ionicPopup,$window,CartService,OfferService){
$scope.subscriptiondata={};
$scope.pcode = [{ id: 0, name: 'Bakery' },{ id: 1, name: 'Dairy' }];
getAdminId=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/getAdminId/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                      var res=response.data;
                                      $scope.subscriptiondata.admin_id=res[0].id;
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );
}
getAdminId();
$scope.search=function(phnumber){
    $http({
      url: '/getCustomerDetails', 
      method: "GET",
      params: {number: phnumber}
         }).then(
                    function(response){
                      $scope.customerdetails = response.data;
                      window.localStorage.customernumber=phnumber;
                      $scope.GetDeliveryTime(phnumber);
                      $scope.customerDetail=$scope.customerdetails[0];
                      $scope.subscriptiondata.customer_mobile=phnumber;
                      if ($scope.customerdetails.length === 0) {
                              ngDialog.openConfirm({template: 'Soory... You have entered Wrong Number.',
                              appendClassName: 'ngdialog-theme-default1',
                              plain: true
                              }).then(function (success) {
                                }, function (error) {
                                });
                        }    else{
                          $scope.GetSelectedDelTime($scope.customerDetail.areaid);
                          }           
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      alert("Wrong phone number.. Please enter correct number");
                    }
                    );

  }; 

$scope.product=[];
$scope.changepcode=function(id){
  $scope.product=[];
  var producturl= id==0 ? '/getFruits':'/getVegetables';
  var productpriceurl= id==0 ? '/getFruitPrices':'/getVegetablePrices';
     var req={
               method: 'GET',
               url: producturl,
               headers: {
                         'Content-Type': 'application/json'
                                      
                         }
             };
    $http(req).then(
                               function(d) {
                                    $scope.products=d.data;
                                    CartService.getListPrices(productpriceurl,$scope.products)
                                    .then(function(res){
                                      $scope.products=res;
                                        angular.forEach($scope.products, function(p){
                                            if(p.price>0){
                                                $scope.product.push(p);
                                            }
                                        });},
                                      function(err){console.log("There is an error ");}) ;
  
                               },
                                function(errResponse){
                                    console.error('Error while fetching vegetablelist');
                                }
                       );

};
$scope.GetDeliveryTime=function(ph){
    $http({method: 'GET', url:'/getDeliveryTime',params: {number: ph}}).then(
    function (res) {
            var time=res.data;
            $scope.timedel=time;                 
            },function(errResponse){
                      console.log(angular.toJson(errResponse));
        });   
};
$scope.GetSelectedDelTime=function(area){
    $scope.alltimings =  _.where( $scope.timedel, { id:parseInt(area)} );
    if ($scope.alltimings.length === 0) {
        $scope.alltimings.push({"id":10,"name":"5:30AM to 10:30AM"});
      }
      var availabledelivertoday=0;
      angular.forEach($scope.alltimings, function(d){
        if (d.name == "Deliver Today") {
          availabledelivertoday=1;

        }     
      });

      if (availabledelivertoday === 0) {
        $scope.alltimings.push({"id":11,"name":"Deliver Today"});
      }
    $scope.time = '3:00 PM';
   var now = new Date();
    var nowTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + now.getHours()+":"+now.getMinutes());
    var userTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + $scope.time);
    if ((nowTime.getTime() > userTime.getTime()) || (nowTime.getTime() == userTime.getTime())) {
               $scope.alltimings.remove("Deliver Today");
    }
  };
  Array.prototype.remove = function(value) {
             this.splice(this.indexOf(value), 1);
             return true;
  };
$scope.changeprice=function(i){ 
  var product =  _.where( $scope.product, { id:parseInt(i)} );
  $scope.subscriptiondata.price =product[0].price;
  $scope.quantity=product[0].quan;
};
$scope.remaingQuantity = function(tprice,price) {
      var value=tprice/price
       $scope.subscriptiondata.remaing_quantity=Math.round(value*100)/100;
 };
 $scope.remaingDiscPriceQuantity = function(discount,tprice,p) {
      var price=p-(p*((discount)/100));
      $scope.subscriptiondata.price =Math.round(price*100)/100;
      var value=tprice/price;
       $scope.subscriptiondata.remaing_quantity=Math.round(value*100)/100;
 };
 $scope.GetEndDate = function(sdate,rquan,quan) {
   var date = new Date(sdate);
   var newdate = new Date(date);
   var daycount=rquan/quan;

   newdate.setDate(newdate.getDate() + daycount - 1);
    
    var dd = newdate.getDate();
    var mm = newdate.getMonth() + 1;
    var yy = newdate.getFullYear();
    if(dd < 10){
        dd = '0'+dd;
    }else{
        dd = dd;
    }
    if(mm < 10){
        mm = '0'+mm;
    }else{
        mm = mm;
    }
    $scope.subscriptiondata.end_date = yy + '-' + mm + '-' + dd; 

 };
GetFrequency=function()
{
      $http.get('/getFrequency')
                            .then(
                                    function(response){
                                        $scope.frequency= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
}
GetFrequency();
$scope.addSubscription = function(subdata) {
      var req={ method: 'GET',url: '/addsubscription',
               headers: {'Content-Type': 'application/json','data':JSON.stringify(subdata)}};
    $http(req)
    .success(function(data) {
     ngDialog.openConfirm({
                    template: 'Thank You !!! Successfully Placed Subscription',
                    appendClassName: 'ngdialog-theme-default1',
                    plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });
    })
    .error(function(data) {
    });
 };
}]);

app.controller('PlaceOrderController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$ionicPopup','$window','CartService','OfferService',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$ionicPopup,$window,CartService,OfferService){
$scope.products=CartService.getCartItems();
$scope.search=function(phnumber){
  $scope.GetDeliveryTime(phnumber);
    $http({
      url: '/getCustomerDetails', 
      method: "GET",
      params: {number: phnumber}
         }).then(
                    function(response){
                      $scope.customerdetails = response.data;
                      window.localStorage.customernumber=phnumber;
                      $scope.customerDetail=$scope.customerdetails[0];
                      if ($scope.customerdetails.length === 0) {
                              ngDialog.openConfirm({template: 'Soory... You have entered Wrong Number.',
                              appendClassName: 'ngdialog-theme-default1',
                              plain: true
                              }).then(function (success) {
                                }, function (error) {
                                });
                        }  else{
                          $scope.GetSelectedDelTime($scope.customerDetail.areaid);
                          }              
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      alert("Wrong phone number.. Please enter correct number");
                    }
                    );

  };
  $scope.GetDeliveryTime=function(ph){
    $http({method: 'GET', url:'/getDeliveryTime',params: {number: ph}}).then(
    function (res) {
            var time=res.data;
            $scope.timedel=time;                 
            },function(errResponse){
                      console.log(angular.toJson(errResponse));
        });   
  };

  $scope.GetSelectedDelTime=function(area){
    $scope.alltimings =  _.where( $scope.timedel, { id:parseInt(area)} );
    if ($scope.alltimings.length === 0) {
        $scope.alltimings.push({"id":10,"name":"5:30AM to 10:30AM"});
      }
      var availabledelivertoday=0;
      angular.forEach($scope.alltimings, function(d){
        if (d.name == "Deliver Today") {
          availabledelivertoday=1;

        }     
      });

      if (availabledelivertoday === 0) {
        $scope.alltimings.push({"id":11,"name":"Deliver Today"});
      }
    $scope.deliveryTime= $scope.alltimings[0];
    $scope.time = '3:00 PM';
   var now = new Date();
    var nowTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + now.getHours()+":"+now.getMinutes());
    var userTime = new Date((now.getMonth()+1) + "/" + now.getDate() + "/" + now.getFullYear() + " " + $scope.time);
    if ((nowTime.getTime() > userTime.getTime()) || (nowTime.getTime() == userTime.getTime())) {
               $scope.alltimings.remove("Deliver Today");
    }
  };
  Array.prototype.remove = function(value) {
             this.splice(this.indexOf(value), 1);
             return true;
  };
      
  

  function getSumofAll(allCartItems){
      
    var sumofAll=0; 
    for(i=0;i<allCartItems.length;i++){
        sumofAll=sumofAll + (allCartItems[i].quantity * allCartItems[i].price);
    }
    //alert(sumofAll);
    return sumofAll;
}
  $scope.increase = function(item) {
        $scope.items =$scope.products;
        $rootScope.updateproductcount=1;
        
        $scope.items.forEach(function(d)
        {
          if(d.id === item.id){
                d.quantity += 1;
                CartService.addCartItem(d);
            }
        })
        $rootScope.sumofAll=getSumofAll($scope.items);
    };

    $scope.quanChanged = function(item) {
        $scope.items =$scope.products;$rootScope.updateproductcount=1;
        
        $scope.items.forEach(function(d)
        {
          if(d.id === item.id){
                CartService.addCartItem(d);
            }
        })
        $rootScope.sumofAll=getSumofAll($scope.items);
    };
    
    $scope.decrease = function(item) {
        $scope.items =$scope.products;$rootScope.updateproductcount=1;
        
        $scope.items.forEach(function(d)
        {
          if(d.id === item.id && d.quantity>0){
                d.quantity -= 1;
                CartService.addCartItem(d);
            }
        })
        $rootScope.sumofAll=getSumofAll($scope.items);
    };
    $scope.GetOffer = function () {
        $scope.offerinfo=JSON.parse(localStorage.getItem('offerdetails'));
        $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
        $scope.discont=JSON.parse(localStorage.getItem('discont'));
            
    };
    $scope.GetOffer();
    $scope.$on('OfferChange', function (event, args) {
        $scope.GetOffer();     
    });
    $scope.offerCheck = function(offer){
      var phnumber=window.localStorage.customernumber;
      var offerdata={'offer':offer,'mobile':phnumber};
        OfferService.getOfferDetails(angular.toJson(offerdata)).then(function(res){
           var info=res;
            $scope.offerdetails=info[0];
            var total=$rootScope.sumofAll;
            var discont;
            if ($scope.offerdetails && $scope.offerdetails.offer_criteria<total) {
            localStorage.setItem('offerdetails', JSON.stringify($scope.offerdetails));
            localStorage.setItem('oldTotal', total);
                if($scope.offerdetails.offer_unit === 'rs'){
                    discont=$scope.offerdetails.offer_amt;
                    total=total-($scope.offerdetails.offer_amt);
                    localStorage.setItem('discont', JSON.stringify(discont));
                    $rootScope.sumofAll=total;
                }
                else
                {
                    discont=(total*(($scope.offerdetails.offer_amt)/100));
                    total=total-(total*(($scope.offerdetails.offer_amt)/100)).toFixed(2);
                    localStorage.setItem('discont', JSON.stringify(discont));
                    $rootScope.sumofAll=total;
                }
            
            $rootScope.$broadcast('OfferChange', { message: "test" });
        }
        else{
            $ionicPopup.alert({
                 title: 'Sorry',
                 template:"This Offer is not applicable for this total Or You have entered invalid coupon code. You can try again later." });
        
        }

        },function(err){$ionicPopup.alert({
                 title: 'Sorry',
                 template:"You have entered invalid coupon code." });
        }) ;

    };
    $scope.sweet = {};
    $scope.sweet.option = {
            title: "Are you sure?",
            text: "You want to remove this product!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, keep it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }
        $scope.sweet.confirm = {
            title: 'Deleted!',
            text: 'Successfully deleted.',
            type: 'success'
        };

        $scope.sweet.cancel = {
            title: 'Cancelled!',
            text: 'Product not deleted.',
            type: 'error'
        };
        
        $scope.checkCancel=function(){
        return !0;
        };
        
         $scope.checkConfirm=function(test){
          return CartService.removeCartItem(test), $scope.products = CartService.getCartItems(), $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal')), !0,$rootScope.updateproductcount=1;
        };

  $scope.showProduct=function(number){
    window.localStorage.customernumber=number;
    //alert(number);
    ngDialog.open({
          template: 'views/product/product.html',
          className: 'ngdialog-theme-default',
          controller: 'productCtrl',
          showClose: false,
          closeByDocument: false,
          closeByEscape: true
      });
  

  };
  $scope.$on('CartChange', function (event, args) {
    $scope.products = CartService.getCartItems(); 
    $rootScope.sumofAll=getSumofAll($scope.products);
    $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
    $rootScope.updateproductcount=1;    
  });
  $scope.go = function(user,deliveryTime) { 
   // alert(angular.toJson(user));alert(deliveryTime);

    CartService.ConfirmOrder(CartService.getCartItems(),user,deliveryTime).then(function(res){
    var total=$rootScope.sumofAll;
    $rootScope.sumofAll=0;
    localStorage.removeItem('lsallCartItems');
    localStorage.removeItem('offerdetails');
    localStorage.removeItem('discont');
    $rootScope.currentOrders='';

    ngDialog.openConfirm({
                    template: 'Thank You !!! Successfully Placed the order',
                    appendClassName: 'ngdialog-theme-default1',
                    plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });

    },function(error){ console.log("error "+ error);});
  };
}]);
app.controller('OrderEditController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$ionicPopup','$window','CartService','OfferService',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$ionicPopup,$window,CartService,OfferService){

$scope.search=function(phnumber){
    $http({
      url: '/getOrderId', 
      method: "GET",
      params: {number: phnumber}
         }).then(
                    function(response){
                      $scope.orders=response.data;   
                      window.localStorage.customernumber=phnumber;
                      if ($scope.orders.length === 0) {
                              ngDialog.openConfirm({template: 'Soory... No Order available for this mobile number.',
                              appendClassName: 'ngdialog-theme-default1',
                              plain: true
                        }).then(function (success) {
                              $window.location.reload();
                          }, function (error) {
                              $window.location.reload();
                          });
                        }              
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      alert("Wrong phone number.. Please enter correct number");
                    }
                    );

  };
  $scope.showCartDetails=function(id){
    $http({
      url: '/getOrderDetails', 
      method: "GET",
      params: {number: id}
         }).then(
                    function(response){
                      var data=response.data;
                      window.localStorage.orderid = data[0].id;
                      $rootScope.updateproductcount=0;
                      window.localStorage.lsallCartItems = data[0].order_detail;
                      $scope.products=JSON.parse(window.localStorage.lsallCartItems);
                      $rootScope.sumofAll=getSumofAll($scope.products);
                      if (data[0].coupon_code == null || data[0].coupon_code == 'No Coupon') {
                        $scope.coupon = 1;
                      }
                      else
                      {
                        $scope.coupon = 2;
                        $scope.offercode=data[0].coupon_code;
                        $scope.oldTotal=data[0].total_before_coupon_apply;
                        localStorage.setItem('oldTotal', $scope.oldTotal);
                        $scope.offerCheck($scope.offercode);
                      }           
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      alert("Please try again later");
                    }
                    );

  };
  function getSumofAll(allCartItems){
      
    var sumofAll=0; 
    for(i=0;i<allCartItems.length;i++){
        sumofAll=sumofAll + (allCartItems[i].quantity * allCartItems[i].price);
    }
    //alert(sumofAll);
    return sumofAll;
}
  $scope.increase = function(item) {
        $scope.items =$scope.products;
        $rootScope.updateproductcount=1;
        
        $scope.items.forEach(function(d)
        {
          if(d.id === item.id){
                d.quantity += 1;
                CartService.addCartItem(d);
            }
        })
        $rootScope.sumofAll=getSumofAll($scope.items);
    };

    $scope.quanChanged = function(item) {
        $scope.items =$scope.products;$rootScope.updateproductcount=1;
        
        $scope.items.forEach(function(d)
        {
          if(d.id === item.id){
                CartService.addCartItem(d);
            }
        })
        $rootScope.sumofAll=getSumofAll($scope.items);
    };
    
    $scope.decrease = function(item) {
        $scope.items =$scope.products;$rootScope.updateproductcount=1;
        
        $scope.items.forEach(function(d)
        {
          if(d.id === item.id && d.quantity>0){
                d.quantity -= 1;
                CartService.addCartItem(d);
            }
        })
        $rootScope.sumofAll=getSumofAll($scope.items);
    };
    $scope.GetOffer = function () {
        $scope.offerinfo=JSON.parse(localStorage.getItem('offerdetails'));
        $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
        $scope.discont=JSON.parse(localStorage.getItem('discont'));
            
    };
    $scope.GetOffer();
    $scope.$on('OfferChange', function (event, args) {
        $scope.GetOffer();     
    });
    $scope.offerCheck = function(offer){
      var phnumber=window.localStorage.customernumber;
      var offerdata={'offer':offer,'mobile':phnumber};
        OfferService.getOfferDetails(angular.toJson(offerdata)).then(function(res){
           var info=res;
            $scope.offerdetails=info[0];
            var total=$rootScope.sumofAll;
            var discont;
            if ($scope.offerdetails && $scope.offerdetails.offer_criteria<total) {
            localStorage.setItem('offerdetails', JSON.stringify($scope.offerdetails));
            localStorage.setItem('oldTotal', total);
                if($scope.offerdetails.offer_unit === 'rs'){
                    discont=$scope.offerdetails.offer_amt;
                    total=total-($scope.offerdetails.offer_amt);
                    localStorage.setItem('discont', JSON.stringify(discont));
                    $rootScope.sumofAll=total;
                }
                else
                {
                    discont=(total*(($scope.offerdetails.offer_amt)/100));
                    total=total-(total*(($scope.offerdetails.offer_amt)/100)).toFixed(2);
                    localStorage.setItem('discont', JSON.stringify(discont));
                    $rootScope.sumofAll=total;
                }
            
            $rootScope.$broadcast('OfferChange', { message: "test" });
        }
        else{
            $ionicPopup.alert({
                 title: 'Sorry',
                 template:"This Offer is not applicable for this total Or You have entered invalid coupon code. You can try again later." });
        
        }

        },function(err){$ionicPopup.alert({
                 title: 'Sorry',
                 template:"You have entered invalid coupon code." });
        }) ;

    };
    $scope.sweet = {};
    $scope.sweet.option = {
            title: "Are you sure?",
            text: "You want to remove this product!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, keep it!",
            closeOnConfirm: false,
            closeOnCancel: false
        }
        $scope.sweet.confirm = {
            title: 'Deleted!',
            text: 'Successfully deleted.',
            type: 'success'
        };

        $scope.sweet.cancel = {
            title: 'Cancelled!',
            text: 'Product not deleted.',
            type: 'error'
        };
        
        $scope.checkCancel=function(){
        return !0;
        };
        
         $scope.checkConfirm=function(test){
          return CartService.removeCartItem(test), $scope.products = CartService.getCartItems(), $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal')), !0,$rootScope.updateproductcount=1;
        };

  $scope.showProduct=function(number){
    window.localStorage.customernumber=number;
    //alert(number);
    ngDialog.open({
          template: 'views/product/product.html',
          className: 'ngdialog-theme-default',
          controller: 'productCtrl',
          showClose: false,
          closeByDocument: false,
          closeByEscape: true
      });
  

  };
  $scope.$on('CartChange', function (event, args) {
    $scope.products = CartService.getCartItems(); 
    $rootScope.sumofAll=getSumofAll($scope.products);
    $scope.oldTotal=JSON.parse(localStorage.getItem('oldTotal'));
    $rootScope.updateproductcount=1;    
  });

  $scope.updateorder = function(phnumber) { 
      console.log("all order data" + JSON.stringify(CartService.getCartItems()));
      CartService.UpdateOrder(CartService.getCartItems(),phnumber).then(function(res){
        ngDialog.openConfirm({template: 'Successfully Updated Order',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                      });
    var total=$rootScope.sumofAll;
    $rootScope.sumofAll=0;
    localStorage.removeItem('lsallCartItems');
    localStorage.removeItem('offerdetails');
    localStorage.removeItem('discont');
    $rootScope.currentOrders='';

    },function(error){ console.log("error "+ error);});
     
     };
}]);

app.controller('adminRegistrationController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$window','AuthService',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$window,AuthService){
$scope.adminDetail={};
GetAllState=function()
    {
      $http.get('/GetAllState')
                            .then(
                                    function(response){
                                        $scope.stateData= response.data;
                                         $scope.selectedState= $scope.stateData[0];

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetAllState();
    $scope.statechange=function(stateid)
      {
        $http({
            url:'/GetMasterCity',
            method: "GET",
            params: {state: stateid}
                }).then(
                                    function(response){
                                        $scope.masterCityName= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
       };
      $scope.citychange=function(cityid)
      {
        $http({
            url:'/GetMasterCityWiseCity',
            method: "GET",
            params: {city: cityid}
                }).then(
                                    function(response){
                                        $scope.cityName= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
        
       };
    GetAllRole=function()
    {
      $http.get('/getAllRole')
                            .then(
                                    function(response){
                                        $scope.roleData= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetAllRole();
    $scope.adminsignup = function(){
      delete $scope.adminDetail.confirmpassword;

      var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
      var ENcodedString = Base64.encode($scope.adminDetail.password);
      $scope.adminDetail.password = ENcodedString;
      /*alert(ENcodedString);
      alert($scope.adminDetail.password);
      alert(angular.toJson($scope.adminDetail));*/
      AuthService.doSignUp(angular.toJson($scope.adminDetail))
      .then(function(data){        
         if(data.insertId && data.insertId!==0){
          ngDialog.openConfirm({template: 'Successfully Inserted Data !!!',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                  });
        }
        else  if(data.indexOf("Mobile Already Exists")!== -1){
          ngDialog.openConfirm({template: 'Sorry... Mobile Number already Exists',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                  });
        }

      }, function(err) { 
        
    });

    };
}]);


app.controller('customerDetailController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$window',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$window){
$scope.customerDetail=[];
GetAllCityName=function()
    {
      $http.get('/GetCityName')
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetAllCityName();
    $scope.citychange=function(cityId)
      {
        $http({
            url:'/GetAreaName',
            method: "GET",
            params: {city: cityId}
                }).then(
                                    function(response){
                                        $scope.areaName= response.data;

                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
       }; 
  $scope.search=function(phnumber){
    $http({
      url: '/getCustomerDetails', 
      method: "GET",
      params: {number: phnumber}
         }).then(
                    function(response){
                      $scope.customerdetails = response.data;
                      $scope.customerDetail=$scope.customerdetails[0];
                      if ($scope.customerdetails.length === 0) {
                              ngDialog.openConfirm({template: 'Soory... You have entered Wrong Number.',
                              appendClassName: 'ngdialog-theme-default1',
                              plain: true
                        }).then(function (success) {
                          }, function (error) {
                          });
                        }
                        else{
                      $scope.citychange($scope.customerDetail.cityid);}                 
                    },
                    function(errResponse){
                      console.error("Error while fetching customer details");
                      alert("Wrong phone number.. Please enter correct number");
                    }
                    );

  };


$scope.updatecustomerdetail=function(data){

  //alert(angular.toJson(data));
  var req={method: 'GET', url:'/updatecustomerdetail',
                     headers: {'Content-Type': 'application/json','data':angular.toJson(data)}
              };
          $http(req).success(function (res) {
                  ngDialog.openConfirm({template: 'Successfully updated customer detail !!!',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                          $window.location.reload();
                      }, function (error) {
                          $window.location.reload();
                  });
                }).error( function(data){
                      console.log(angular.toJson(data));
                  });
  };

}]);
app.controller('assignSubscriptionController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$window',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$window){
GetRoleWiseCityName=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/GetRoleWiseCityName/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
}
GetRoleWiseCityName();
$scope.change=function(data){
      $scope.citychange=data;
      $http({
            url:'/GetDeliveryBoyName',
            method: "GET",
            params: {city: $scope.selectedcity}
                }).then(
                                    function(response){
                                        $scope.delboydetls= response.data;
                                        //alert(angular.toJson($scope.delboydetls));
                                        $scope.selecteddelboydetls= $scope.delboydetls[0];
                                        if($scope.delboydetls.length == 0)
                                        {
                                          ngDialog.openConfirm({template: 'Sorry there is no delivery boy assigned to this city!!!',
                                                                appendClassName: 'ngdialog-theme-default1',
                                                                plain: true
                                                          }).then(function (success) {
                                                            }, function (error) {
                                                        });
                                        }
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );

      getPendingSubscriptionOrders();
}
getPendingSubscriptionOrders = function(){
    $http({
    url: '/getPendingSubOrders', 
    method: "GET",
    params: {city: $scope.selectedcity}
       }).then(
                  function(response){
                    sessionStorage.pendingSubOrders = angular.toJson(response.data);
                    $scope.pendingSubOrders = response.data;
                    
                  },
                  function(errResponse){
                    console.error("Error while fetching pending orders");
                    
                  }
                  );
        }
getPendingSubscriptionOrders();

  $scope.savedelboydata=  function()
        { 
          if ($scope.selecteddelboydetls.name==null) {
            ngDialog.openConfirm({template: 'Please assign delivery boy!!',
                          appendClassName: 'ngdialog-theme-default1',
                          plain: true
                    }).then(function (success) {
                      }, function (error) {
                  });
          }
          else
          {
            var checkedId=[];
            angular.forEach($scope.pendingSubOrders, function(value, index){
                
                if(value.isChecked){
                            checkedId.push([value.id,$scope.selecteddelboydetls.id]);
                }
            });
                       var req={
                        method: 'POST',
                        url: '/SetSubscriptionOrderDeliverBoy',
                        headers: {
                              'Content-Type': 'application/json', data:angular.toJson(checkedId)
                         }};
                              
                          $http(req).then(
                                                     function(d) {
                                                          ngDialog.openConfirm({template: 'Data inserted successfully!!',
                                                                appendClassName: 'ngdialog-theme-default1',
                                                                plain: true
                                                          }).then(function (success) {
                                                                $window.location.reload();
                                                            }, function (error) {
                                                                $window.location.reload();
                                                        });
                                                     },
                                                      function(errResponse){
                                                          console.error('Error while insertng Vegetable data');
                                                      }
                                );


          }
        } 

}]);
app.controller('deliveryController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','$window',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,$window){

GetRoleWiseCityName=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/GetRoleWiseCityName/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    }
    GetRoleWiseCityName();

    $scope.change=function(data){
      $scope.citychange=data;
      $http({
            url:'/GetDeliveryBoyName',
            method: "GET",
            params: {city: $scope.selectedcity}
                }).then(
                                    function(response){
                                        $scope.delboydetls= response.data;
                                        //alert(angular.toJson($scope.delboydetls));
                                        $scope.selecteddelboydetls= $scope.delboydetls[0];
                                        if($scope.delboydetls.length == 0)
                                        {
                                         // $scope.disable=true;
                                          alert("Sorry there is no delivery boy assigned to this city!!!");
                                        }
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );

      getPendingOrders();
    }

    getPendingOrders = function(){
    $http({
    url: '/getPendingOrders', 
    method: "GET",
    params: {city: $scope.selectedcity}
       }).then(
                  function(response){
                    sessionStorage.pendingOrders = angular.toJson(response.data);
                    $scope.pendingOrders = response.data;
                   //alert(angular.toJson($scope.pendingOrders));
                    
                  },
                  function(errResponse){
                    console.error("Error while fetching pending orders");
                    
                  }
                  );
        }
      getPendingOrders();

        $scope.showName=function(data){
          var cust=JSON.parse(data);
         // console.log(cust.name);
          return cust.name;

        };
         $scope.showMobile=function(data){
          return ( JSON.parse(data).address + " \n"+JSON.parse(data).mobile);
        }; 

        $scope.savedelboydata=  function()
        { 
        //alert (angular.toJson($scope.selecteddelboydetls));
          if ($scope.selecteddelboydetls.name==null) {
            alert("Please assign delivery boy!!");
          }
          else
          {
            var checkedId=[];
            angular.forEach($scope.pendingOrders, function(value, index){
                
                if(value.isChecked){
                            checkedId.push([value.id,$scope.selecteddelboydetls.id]);
                }
            });

            //alert(angular.toJson(checkedId));

                       var req={
                        method: 'POST',
                        url: '/SetOrderDeliverBoy',
                        headers: {
                              'Content-Type': 'application/json', data:angular.toJson(checkedId)
                         }};
                              
                          $http(req).then(
                                                     function(d) {
                                                          $scope.vegData={};
                                                          console.log(angular.toJson(d));
                                                          alert("Data inserted successfully!!");
                                                          $window.location.reload();
                                                     },
                                                      function(errResponse){
                                                          console.error('Error while insertng Vegetable data');
                                                      }
                                );


          }
        }   

}]);

app.controller('vegdetailscontroller',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$window',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$window){

var name=$routeParams.name;
$scope.inventryname= name=='dairy' ? 'Dairy':'Bakery';
$scope.VegDetails;
//alert(name);
 GetRoleWiseCityName=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/GetRoleWiseCityName/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );
    }
    GetRoleWiseCityName();

    var urlname= name=='dairy' ? '/GetVegDetails':'/GetFruitsDetails';
    var cdate=$filter('date')(new Date(), 'yyyy-MM-dd');

    GetAllVegDetails=function()
    {
      $http({
        url:urlname,
        method: "GET",
        params: {city: $scope.selectedcity}
            }).then(
                                    function(response){
                                        $scope.VegDetails= response.data;
                                        $scope.VegDetails.forEach(function(obj){
                                          obj.supplier_price=0;
                                          obj.todays_price=obj.price;
                                        });
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );
    }
    GetAllVegDetails();


    var checkurl= name=='dairy' ? '/CheckVegUpdatedRate':'/CheckFruitUpdatedRate';  
    $scope.change=function(data){
      $rootScope.citychange=data;
      $http({
            url:checkurl,
            method: "GET",
            params: {city: $scope.selectedcity, date:cdate}
                }).then(
                                    function(response){
                                        $scope.count= response.data;
                                        //alert(angular.toJson($scope.count));
                                        if($scope.count[0]["count(*)"]=='0')
                                        {
                                          GetAllVegDetails();
                                        }
                                        else
                                        {
                                          alert("Sorry you have updated price for today!!!");
                                        }
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );

}
    
$scope.vegData=[];
$scope.todaysdate;
var saveurlname= name=='dairy' ? '/InsertVegDetails':'/InsertFruitDetails';

$scope.savevegdata = function(alldata,index){ 
  $scope.finaldata=[];
  todaysdate=$filter('date')(new Date(), 'yyyy-MM-dd');
  //vegData=angular.toJson(alldata);
      alldata.forEach(function(obj){
    delete obj.image;
    delete obj.hindi_name;
    obj.date=todaysdate;
    obj.city=$rootScope.citychange;
  });
      //alert(angular.toJson(alldata));
      alldata.forEach(function(data){
        if(data.todays_price || data.todays_price == 0){
          $scope.finaldata.push(data);
        }
      });
      //alert(angular.toJson($scope.finaldata));
  if ($rootScope.citychange) {
                        var req={
                        method: 'POST',
                        url: saveurlname,
                        headers: {
                              'Content-Type': 'application/json', data:angular.toJson($scope.finaldata)
                         }};
                              
                          $http(req).then(
                                                     function(d) {
                                                          $scope.vegData={};
                                                          console.log(angular.toJson(d));
                                                          ngDialog.openConfirm({template: 'Data inserted successfully!! !!!',
                                                                  appendClassName: 'ngdialog-theme-default1',
                                                                  plain: true
                                                            }).then(function (success) {
                                                                  $window.location.reload();
                                                              }, function (error) {
                                                                  $window.location.reload();
                                                          });
                                                     },
                                                      function(errResponse){
                                                          console.error('Error while insertng Vegetable data');
                                                      }
                                );
                       }
                       else
                       {
                        alert("Please Select Ur city");
                       }
}

 }]);

app.controller('mastervegdatacontroller',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$window',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$window){

var name=$routeParams.name;
$scope.inventryname= name=='dairy' ? 'Dairy':'Bakery';

GetRoleWiseCityName=function()
    {
      var data=$rootScope.globals.currentUser.username;
      $http.get('/GetRoleWiseCityName/'+ angular.toJson(data))
                            .then(
                                    function(response){
                                        $scope.cityName= response.data;
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                        
                                    }
                            );
    }
    GetRoleWiseCityName();

    var urlname= name=='dairy' ? '/GetMasterVegDetails':'/GetMasterFruitDetails';

    GetAllMasterVegDetails=function(city,date)
    {
      $http({
        url:urlname,
        method: "GET",
        params: {'city': city, 'date':date}
            })
                            .then(
                                    function(response){
                                        $scope.MasterVegDetails= response.data;
                                        if ($scope.MasterVegDetails.length == 0) {
                                          ngDialog.openConfirm({template: 'No Data available!!!',
                                                                  appendClassName: 'ngdialog-theme-default1',
                                                                  plain: true
                                                            }).then(function (success) {
                                                                  $window.location.reload();
                                                              }, function (error) {
                                                                  $window.location.reload();
                                                          });
                                        }
                                      
                                    }, 
                                    function(errResponse){
                                        console.error('Error while fetching followups');
                                    }
                            );
    };

    $scope.city;  
    $scope.search=function(city,date){
      $scope.date=$filter('date')(date, "yyyy-MM-dd");
      
      GetAllMasterVegDetails(city,$scope.date);
      //alert(city);
    }

    var saveurlname= name=='dairy' ? '/UpdateMasterVegDetails':'/UpdateMasterFruitDetails';
    var updateurlname= name=='dairy' ? '/UpdateMaterStatus':'/UpdateMaterFruitStatus';

    $scope.saveallvegdata = function(alldata,index){ 

      alldata.forEach(function(obj){
        delete obj.id;
        delete obj.name;
        delete obj.price;
        delete obj.hindi_name;
        delete obj.supplier_price;
      });
      var req={
      method: 'POST',
      url: saveurlname,
      headers: {
            'Content-Type': 'application/json', data:angular.toJson(alldata)
       }};
            
        $http(req).then(
                                   function(d) {
                                        $scope.vegData={};
                                        console.log(angular.toJson(d));
                                        
                                        var req={
                                          method: 'POST',
                                          url: updateurlname,
                                          headers: {
                                                'Content-Type': 'application/json', data:angular.toJson(alldata)
                                           }};
                                                
                                            $http(req).then(
                                                   function(d) {
                                                        $scope.vegData={};
                                                        console.log(angular.toJson(d));
                                                        
                                                        ngDialog.openConfirm({template: 'Data inserted successfully!! !!!',
                                                                  appendClassName: 'ngdialog-theme-default1',
                                                                  plain: true
                                                            }).then(function (success) {
                                                                  $window.location.reload();
                                                              }, function (error) {
                                                                  $window.location.reload();
                                                          });


                                                   },
                                                    function(errResponse){
                                                        console.error('Error while insertng Vegetable status');
                                                    }
                                           );


                                   },
                                    function(errResponse){
                                        console.error('Error while insertng Vegetable data');
                                    }
                           );
    }
}]);

app.controller('LogoutController',['$rootScope','$filter','$scope','$http','$timeout','$routeParams','ngDialog','$window','AuthenticationService',function($rootScope,$filter,$scope,$http,$timeout,$routeParams,ngDialog,$window,AuthenticationService){
var logout=function() {
    AuthenticationService.ClearCredentials();
    localStorage.removeItem('customernumber');
   localStorage.removeItem('lsallFruits');
   localStorage.removeItem('lsallVegetables');
   localStorage.removeItem('oldTotal');
   localStorage.removeItem('orderid');
   $window.location.reload();
   $location.path('/login');
};
$timeout(function () {logout();},100);
}]);



app.controller('LoginController', LoginController);
 
    LoginController.$inject = ['$location', 'AuthenticationService','$scope' ];
    function LoginController($location, AuthenticationService,$scope) {
        var vm = this;
        $scope.username ='';
        $scope.password='';
        $scope.result='';
     
        //vm.login = login;
 
        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();
 
        $scope.login=function() {
            vm.dataLoading = true;
            $scope.result='';
            AuthenticationService.Login( $scope.username, $scope.password, function (response) {
              console.log(JSON.stringify(response));
                if (response.success) {
                	//alert(response);
                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                    $location.path('/');
                } else {
                	//alert("fail "+response);
                    //$scope.result="Invalid user id or Password";
                    angular.element('#result').addClass('alert alert-danger');
                     angular.element('#result').html("Invalid user id or Password");
                    vm.dataLoading = false;
                }
            });
        };
    }
